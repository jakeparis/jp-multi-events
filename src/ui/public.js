
import './public.scss';

document.addEventListener('DOMContentLoaded', function(){
	// assumes only one on a page
	var upcoming = document.querySelector('.jpme-event-single-view-upcoming-dates-wrap');
	if( ! upcoming )
		return;
	var a = upcoming.querySelector('.js-opener');
	var list = upcoming.querySelector('ul');
	a.addEventListener('click',function(e){
		e.preventDefault();
		a.classList.add('js-active');
		list.classList.add('js-active');
	});
	list.addEventListener('click',function(e){
		e.preventDefault();
		a.classList.remove('js-active');
		list.classList.remove('js-active');
	});
});
