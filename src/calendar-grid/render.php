<?php

// $attributes, $content


$Plugin = Jp_Multi_Events::instance();
$Plugin->get_blocks_manager()->enqueuePublicJs( 'jp-multi-events/calendar-grid' );

if ( isset( $attributes['displayFilterBar'] ) ) {
	$attributes['showFilters'] = $attributes['displayFilterBar'];
	unset( $attributes['displayFilterBar'] );
}
if ( isset( $attributes['onlyTag'] ) ) {
	$attributes['tag'] = $attributes['onlyTag'];
	unset( $attributes['onlyTag'] );
}

$attributes['date'] = ( isset( $_GET['jpme_date'] ) ) ? $_GET['jpme_date'] : 'today';

echo $Plugin->print_calendar( $attributes );
