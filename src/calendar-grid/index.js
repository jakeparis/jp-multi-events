
import save from './save';
import edit from './edit';
import icon from '../../static/calendar2.svg';

import {
	registerBlockType
} from '@wordpress/blocks';

import './index.scss';

import blockData from './block.json';

registerBlockType( blockData.name, {
	...blockData,
	icon: <img src={icon} />,
	edit,
	save,
});
 