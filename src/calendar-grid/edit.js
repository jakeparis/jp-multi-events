
import {
	InspectorControls,
	useBlockProps,
} from '@wordpress/block-editor';

import ServerSideRender from '@wordpress/server-side-render';

import {
	PanelBody,
	TextControl,
	ToggleControl,
} from '@wordpress/components';

export default function edit (props) {

	const { attributes, setAttributes, isSelected } = props;

	const blockProps = useBlockProps();

	return (

		<div {...blockProps}>

			<InspectorControls>
				<PanelBody title="Calendar grid settings">
					<TextControl
						label="Only show events with this tag"
						value={ attributes.onlyTag }
						onChange={onlyTag=>setAttributes({onlyTag}) }
					/>
					<ToggleControl
						label="Display the filter bar above the calendar"
						checked={ attributes.displayFilterBar }
						onChange={ displayFilterBar=>setAttributes({displayFilterBar}) }
					/>
					{ attributes.displayFilterBar && (
						<TextControl
							label="Display only these tags on the filter bar"
							help="The tags must match an existing tag to be recognized here"
							value={ attributes.onlyShowTheseTagsInFilterList }
							onChange={ v=>setAttributes({onlyShowTheseTagsInFilterList:v}) }
						/>
					)}
				</PanelBody>
			</InspectorControls>

			<div className="calendar-grid-preview" >
				<ServerSideRender block="jp-multi-events/calendar-grid"
					attributes={ attributes }
				/>
			</div>
		</div>
	);
}
