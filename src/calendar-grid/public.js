
import './public.scss';

jQuery(function($){

	// Sorry, this is a nasty piece of code

	var getEventsFromList = function( checkboxEl ){
		if( $(checkboxEl).parents('.jpme-cal-grid').length )
			return $(checkboxEl).parents('.jpme-cal-grid').find('.jpme-cal-grid_item');
		else if ( $(checkboxEl).parents('.events-list').length )
			return $(checkboxEl).parents('.events-list').find('.events-list-single-event');
		else if ( $(checkboxEl).parents('.event-tag-filters').next('.events-list').length )
			return $(checkboxEl).parents('.event-tag-filters').next('.events-list').find('.events-list-single-event');
		else
			return [];
	};

	var showEventsTagged = function(tag, events){
		events.each(function(){
			var tags = $(this).data('tags');
			if( ! tags) {
				$(this).hide();
			} else {
				if( tags.indexOf(tag) > -1 ) {
					$(this).show();
				}
			}
		});
	};

	/**
	 * 
	 * @param {string} url The url to work with
	 * @param {string} key The param key to modify
	 * @param {mixed} value The value to set the key to. If === false, the key will be deleted
	 * @returns {string}
	 */
	var setParamInUrl = function(url, key, value) {
		var url = new URL(url);
		var p = new URLSearchParams(url.search)
		if( value === false )
			p.delete( key );
		else
			p.set( key, value )
		url.search = p.toString()
		return url.href;
	}

	var updateUrlsWithTags = function(tagNames) {
		// update the history state
		var newloc;
		var nextLink = document.querySelector('.jpme-cal-grid_navNext');
		var prevLink = document.querySelector('.jpme-cal-grid_navPrev');
		var homeLink = document.querySelector('.jpme-cal-returnToToday');

		tagNames = (tagNames.length) ? tagNames.join(',') : false;

		var newloc = setParamInUrl(window.location.href, 'jpme_tagFilters', tagNames)
		window.history.pushState({}, null, newloc );
		
		if( nextLink ) {
			nextLink.setAttribute( 'href', 
				setParamInUrl( nextLink.getAttribute('href'), 'jpme_tagFilters', tagNames )
			)
		}
		if( prevLink ) {
			prevLink.setAttribute( 'href', 
				setParamInUrl( prevLink.getAttribute('href'), 'jpme_tagFilters', tagNames )
			)
		}
		if( homeLink ) {
			homeLink.setAttribute( 'href', 
				setParamInUrl( homeLink.getAttribute('href'), 'jpme_tagFilters', tagNames )
			)
		}

	};

	var checkEventFilterState = function( checkboxEl ){
		var $checkboxes, ev;
		if( ! checkboxEl ){
			$checkboxes = $('.event-tag-filters input[type="checkbox"]');
			if( ! $checkboxes.length )
				return;
			checkboxEl = $checkboxes.get(0)
		} else {
			$checkboxes = $(checkboxEl).parents('.event-tag-filters').find('input[type="checkbox"]');
		}
		
		if( $checkboxes )
			ev = getEventsFromList( checkboxEl );
		if( ! ev.length )
			return;
		
		var filtersToSave = [];

		if( $checkboxes.filter(':checked').length < 1 ) {
			ev.show();

		} else {
			// hide all
			ev.hide();

			$checkboxes.filter(':checked').each(function(){
				showEventsTagged( $(this).val(), ev);
				filtersToSave.push( $(this).val() );
			});
		}

		updateUrlsWithTags( filtersToSave );
	};

	$('.event-tag-filters input[type="checkbox"]').on('change',function(){
		checkEventFilterState( this );
	});

	// on initial load, if any filters are selected, hide stuff as needed
	checkEventFilterState();


});
