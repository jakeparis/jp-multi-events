
import apiFetch from '@wordpress/api-fetch';
import { useState, useEffect } from '@wordpress/element';
import {
	Notice,
	TextControl,
	Button,
	Spinner,
	Modal,
} from '@wordpress/components';

export default function MapSelector ( props ) {
	const { address, coordinates, onChangeCoordinates=()=>{}, onChangeAddress=()=>{}, updateAddressOnSelect=false } = props;

	const [ isFetchingLocations, setIsFetchingLocations ] = useState(false);
	const [ locationsFetchErr, setLocationsFetchErr ] = useState();
	const [ locationsResp, setLocationsResp ] = useState([])
	const [ mapImageUrl, setMapImageUrl ] = useState(null)

	useEffect(() => {
		if ( ! coordinates ) {
			setMapImageUrl('')
			return;
		}
		const body = new URLSearchParams({
			coords: coordinates
		})
		apiFetch({
			method: 'POST',
			body,
			path: 'jp-events/v1/getmap/',
		}).then( resp => {
			if ( ! resp ) {
				setMapImageUrl(null)	
			} else {
				setLocationsFetchErr(false);
				setLocationsResp([]);
				setMapImageUrl( resp );
			}
		});
	}, [ coordinates ] );


	const lookupAddress = async () => {
		if ( address == '' ) {
			setLocationsResp = [];
			return;
		}
		setIsFetchingLocations(true);
		const params = new URLSearchParams({
			lookupaddress: address
		})
		const locations = await apiFetch({
			path: `jp-events/v1/lookupaddress/?` + params.toString(),
		});
		if ( ! locations.length ) {
			setLocationsFetchErr( 'No locations found' );
			setLocationsResp([])
		// } else if ( locations.length == 1 ) {
		// 	setLocationsFetchErr(false);
		// 	setLocationsResp( locations );
		} else {
			setLocationsFetchErr(false);
			setLocationsResp( locations );
		}
		setIsFetchingLocations(false);
	}

	return (
		<>
			{ locationsFetchErr && (
				<Notice status="error" onDismiss={()=>setLocationsFetchErr(false)}>{ locationsFetchErr }</Notice>
			)}
			<div className="columns">
				<div>
					<TextControl
						label="Location"
						value={ address }
						onChange={ onChangeAddress }
						placeholder="123 Main St, Baltimore, MD"
					/>
				</div>
				<div className="map-column">
					{ ! coordinates && locationsResp.length ==0 && ! isFetchingLocations && (
						<Button 
							variant="secondary" 
							onClick={ lookupAddress } 
							isBusy={ isFetchingLocations }
							className="lookup-address"
						>Add a map/directions link</Button>
					)}
					{ ! coordinates && locationsResp.length ==0 && isFetchingLocations && (
						<Spinner />
					)}
					{ coordinates && (
						<Button 
							href="#" 
							onClick={ ()=>onChangeCoordinates('') }
							variant='tertiary'
							isDestructive={true}
						>Clear map</Button>
					)}

					{ locationsResp.length > 0 && !coordinates && (
						<Modal title="Choose a Map Location"
							onRequestClose={ () => {
								setLocationsResp([])
							}}
							>
							<ul>
								{ locationsResp.map( loc => (
									<li key={ loc.id }>
										<a href="#" onClick={ () => {
											setLocationsResp([]);
											onChangeCoordinates( JSON.stringify( loc.position ) );
											if ( updateAddressOnSelect )
												onChangeAddress( loc.address.label );
										}}>{ loc.address.label }</a>
									</li>
								))}
							</ul>
							<Button variant="secondary" onClick={()=>setLocationsResp([])}>Cancel</Button>
						</Modal>
					)}
					{ mapImageUrl && (
						<img src={ mapImageUrl } />
					)}
				</div>
			</div>
		</>
	)

}