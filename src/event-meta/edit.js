
import { useBlockProps, } from '@wordpress/block-editor';
import { useRef } from '@wordpress/element';
import { useEntityProp, } from '@wordpress/core-data';

import {
	TextareaControl,
	TextControl,
	CheckboxControl,
	Dashicon,
	Button,
	Placeholder,
} from '@wordpress/components';

import Flatpickr from "react-flatpickr";
import 'flatpickr/dist/themes/material_orange.css';
import keyBy from 'lodash.keyby';
import MapSelector from './MapSelector';

export default function edit (props) {

	const blockProps = useBlockProps();
	const [ meta, setMeta ] = useEntityProp( 'postType', 'event', 'meta' );
	const flatPickrInstance = useRef(null);
	const calButton = useRef(null);

	const eventDates = meta["_Event Date" ] ?? [];
	const eventTime = (meta) ? meta["_Event Time" ] : '';
	const eventEndTime = (meta) ? meta["_EventEndTime" ] : '';
	const eventGroupOccasions = (meta) ? meta["_Event Group Occasions" ] : false;
	const eventLocation = (meta) ? meta["_event_location"] : '';
	const eventLocationCoords = (meta) ? meta["_event_location_coords"] : '';


	// const formatDatePretty = ( dateString ) => {
	// 	if( ! dateString )
	// 		return "";
	// 	try {
	// 		let d = new Date( `${dateString} 00:00` );
	// 		return new Intl.DateTimeFormat('en-US', 
	// 			{ year: 'numeric', month: 'long', day: 'numeric' }
	// 			).format(d);
	// 	} catch (e) {
	// 		return dateString;
	// 	}
	// };
	const formatDateForMachine = ( dateObj ) => {
		let dt = new Intl.DateTimeFormat('en-US', { year: 'numeric', month: '2-digit', day: '2-digit' }).formatToParts(dateObj);
		dt = keyBy( dt, 'type' );
		return `${dt.year.value}-${dt.month.value}-${dt.day.value}`;
	};

	const clearOldDates = () => {
		const today = new Date();
		let selectedDates = flatPickrInstance.current.flatpickr.selectedDates;
		let upcomingOnly = selectedDates.filter( d => d >= today );
		upcomingOnly = upcomingOnly.map( formatDateForMachine );
		updateMetaVal( '_Event Date', upcomingOnly );
	};

	const updateMetaVal = (metaname,val) => {
		return setMeta({
			...meta,
			[metaname]: val,
		});
	};

	const updateDatesFromString = (datesStr, splitOn="\n") => {
		var dates = datesStr.split( splitOn );
		updateMetaVal('_Event Date', dates );
	};


	return (
		<div {...blockProps}>
			<Placeholder
				icon={ <Dashicon icon="calendar" /> }
				label="Event Date & Location"
			>
				<div className="columns">
					<div>
						<TextareaControl
							label="Event Dates"
							help="Put one date on each line. Any date format should work."
							value={ eventDates.join("\n") }
							onChange={ updateDatesFromString }
							height="11em"
						/>
						<div className='button-group'>
							<Flatpickr ref={flatPickrInstance}
								style={{display:'none'}}
								value={ eventDates }
								options={{
									mode: "multiple",
									conjunction: '|',
									onChange: function( selectedDates, dateStr ){
										updateDatesFromString( dateStr, "|" );
									},
									dateFormat: "Y-m-d",
									showMonths: 3,
									// defaultDate: eventDates,
									positionElement: calButton.current,
									clickOpens: false, // off so we can toggle with the button
								}}
							/>
							<Button
								ref={ calButton }
								onClick={ ()=>flatPickrInstance.current.flatpickr.open() }
								icon={ 
									<Dashicon icon="calendar-alt" /> 
								}
								variant="secondary"
							>Select from calendar</Button>

							<Button	
								href="#"
								onClick={ clearOldDates }
								variant='tertiary'
								isDestructive={true}
							>Clear old/past dates</Button>
						</div>
					</div>
					<div>
						<TextControl
							label="Start Time"
							value={ eventTime }
							onChange={ v => updateMetaVal('_Event Time', v) }
						/>
						<TextControl
							label="End Time"
							value={ eventEndTime }
							onChange={ v => updateMetaVal('_EventEndTime', v) }
						/><CheckboxControl
							label="Group multiple instances in lists"
							help="When checked, only the nearest event will show in lists. If unchecked, the event listing will repeat for each date held."
							checked={ eventGroupOccasions }
							onChange={ b => updateMetaVal('_Event Group Occasions', b) }
						/>
					</div>
				</div>
				<hr style={{ width: '100%' }} />
				
				<MapSelector
					coordinates={ eventLocationCoords }
					address={ eventLocation }
					onChangeAddress={ v => updateMetaVal( '_event_location', v )} 
					onChangeCoordinates={ v => updateMetaVal( '_event_location_coords', v )}
					updateAddressOnSelect={ false }
				/>
		
			</Placeholder>
			
		</div>
	);
}