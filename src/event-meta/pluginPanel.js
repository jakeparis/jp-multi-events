
import {
	PluginSidebar,
	PluginSidebarMoreMenuItem,
} from '@wordpress/edit-post';

import { calendar } from '@wordpress/icons';
import { createBlock } from '@wordpress/blocks';

import { PanelBody } from '@wordpress/components';
import { useMemo } from '@wordpress/element';

import { useSelect, useDispatch } from '@wordpress/data';


const pluginPanel = {
	icon: calendar,
	render: function () {
		const sidebarTitle = "Event Controls";

		const postType = useSelect( select => select('core/editor').getCurrentPostType(), []);
		
		const blocks = useSelect( select => select('core/block-editor').getBlocks() );
		const blockEditorDispatch = useDispatch('core/block-editor');

		const firstBlock = useMemo( () => {
			if( typeof blocks === 'undefined' )
				return {}
			return blocks[0];
		}, [ blocks ]);

		if( ! postType || postType != 'event' )
			return null;		

		const noNeedText = ( firstBlock?.name === "jp-multi-events/event-meta" )
			? ( <p>Use the Event block in the main editor area to adjust the event's date and time.</p> ) : null;

		const insertEventMetaBlock = () => {
			blockEditorDispatch.insertBlock( 
				createBlock( "jp-multi-events/event-meta" ),
				0
			);
		};

		return ( <>
			<PluginSidebar
				name="jp-multi-events-meta-sidebar"
				icon={ calendar }
				title={sidebarTitle}
				className="plugin-jp-multi-events-meta-sidebar"
			>
				<PanelBody title="Event Settings">
					{ noNeedText || ( <>
						<p>If the Event Date/Time settings box has disappeared (or 
							if you are viewing an event created before version 4), make it visible again:
						</p>

						<p><a href="#" onClick={ insertEventMetaBlock }>Show Event date/time</a></p>
					</>)}
				</PanelBody>
			</PluginSidebar>


			<PluginSidebarMoreMenuItem
				target="jp-multi-events-meta-sidebar"
				icon={ calendar }
			>
				{sidebarTitle}
			</PluginSidebarMoreMenuItem>

		</> );
	},
}

export default pluginPanel;