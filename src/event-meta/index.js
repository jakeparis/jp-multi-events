
import edit from './edit';
import pluginPanel from './pluginPanel';
import icon from '../../static/calendar2.svg';

import {
	registerBlockType
} from '@wordpress/blocks';

import {
	registerPlugin,
} from '@wordpress/plugins';

import './index.scss';

import blockData from './block.json';

registerBlockType( blockData.name, {
	...blockData,
	edit,
	icon: <img src={icon} />,
});
 

registerPlugin( 'jp-multi-events-meta-sidebar', pluginPanel );
