
import {
	InspectorControls,
	useBlockProps,
} from '@wordpress/block-editor';

import ServerSideRender from '@wordpress/server-side-render';

import {
	PanelBody,
	RadioControl,
	TextControl,
	CheckboxControl,
	__experimentalNumberControl as NumberControl,
} from '@wordpress/components';

import {
	useState,
	useEffect,
} from '@wordpress/element';

import PreventClicks from '../PreventClicks';

export default function edit ( props ) {
	
	const { attributes, setAttributes } = props;

	const blockProps = useBlockProps();

	// const [ toHide, setToHide ] = useState( attributes.hide_parts.split(',') );

	const to_hide = attributes.hide_parts.length 
		? attributes.hide_parts.split(',')
		: [];

	const updateHide = ( field, b ) => {
		let _hide = [...to_hide];
		if ( b === true && ! _hide.includes(field)) {
			_hide.push( field );
		}
		if ( b === false && _hide.includes(field) ) {
			let key = _hide.findIndex( f => f == field );
			delete _hide[key];
		}
		_hide = _hide.filter( v => v != '' );
		setAttributes({
			hide_parts: _hide.join(','),
		})
	};

	return (

		<div {...blockProps}>

			<InspectorControls>
				<PanelBody title="Events list settings">
					<TextControl
						label="Only show events with these tags"
						help="A single tag, or a comma-separated list of tags"
						value={ attributes.onlyTags }
						onChange={onlyTags=>setAttributes({onlyTags}) }
					/>
					<NumberControl
						label="Limit # of events displayed"
						value={ attributes.limit }
						onChange={ limit => setAttributes({limit}) }
						required={true}
						min={1}
						max={50}
					/>
					<RadioControl
						label="Display type"
						options={[
							{
								label: 'Long',
								value: 'long',
							},{
								label: 'Short',
								value: 'short',
							},{
								label: 'Just text',
								value: 'text',
							}]
						}
						selected={ attributes.list_type }
						onChange={ list_type => setAttributes({list_type}) }
					/>
					<CheckboxControl
						label="Display paging"
						checked={ attributes.show_paging }
						onChange={ show_paging => setAttributes({ show_paging })}
					/>
					<RadioControl
						label="Past or Future Events"
						options={[
							{
								label: "Upcoming",
								value: "future"
							},{
								label: "Past",
								value: "past"
							}
						]}
						selected={ attributes.futureOrPast }
						onChange={ futureOrPast => setAttributes({futureOrPast}) }
					/>
					<TextControl
						label="Text to display if there are no events"
						value={ attributes.no_events_text }
						onChange={ no_events_text => setAttributes({no_events_text})}
					/>
				</PanelBody>
				{ attributes.list_type != 'text' && (
					<PanelBody title="Sections to Hide">
						<CheckboxControl
							label="Title"
							checked={ to_hide.includes( 'title' ) }
							onChange={ b => updateHide( 'title', b )}
						/>
						<CheckboxControl
							label="Date"
							checked={ to_hide.includes( 'date' ) }
							onChange={ b => updateHide( 'date', b )}
						/>
						<CheckboxControl
							label="Description"
							checked={ to_hide.includes( 'description' ) }
							onChange={ b => updateHide( 'description', b )}
						/>
						<CheckboxControl
							label="Location"
							checked={ to_hide.includes( 'location' ) }
							onChange={ b => updateHide( 'location', b )}
						/>
						<CheckboxControl
							label="Tags"
							checked={ to_hide.includes( 'tags' ) }
							onChange={ b => updateHide( 'tags', b )}
						/>
						<CheckboxControl
							label="Thumbnail"
							checked={ to_hide.includes( 'thumbnail' ) }
							onChange={ b => updateHide( 'thumbnail', b )}
						/>

					</PanelBody>
				)}
			</InspectorControls>

			<PreventClicks>
				<ServerSideRender block="jp-multi-events/events-list"
					attributes={ attributes }
				/>
			</PreventClicks>
		</div>
	);
}
