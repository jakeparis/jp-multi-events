
export default function PreventClicks ( props ) {

	return (
		<div style={{ pointerEvents: 'none' }}>
			{ props.children }
		</div>
	)

}