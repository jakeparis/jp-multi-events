<?php

class Jp_Multi_CalendarGrid {

	public array $events = [];

	private const FIRST_DAY = 0;
	private const DAY_NAMES = [
		'Sunday',
		'Monday',
		'Tuesday',
		'Wednesday',
		'Thursday',
		'Friday',
		'Saturday',
	];

	private int $dayNameDisplayLength = 3;

	private $date;
	private $prevYear;
	private $prevMonth;
	private $nextYear;
	private $nextMonth;
	private $timezone;
	private $today = '';
	private $linkOriginal = '';
	private $linkNextMonth = '';
	private $linkPrevMonth = '';

	/**
	 * Create a new calendar grid
	 *
	 * @param int $month The number of the month
	 * @param int $year  The number of the year
	 * @param array $args Various options:
	 *
	 * @params int $args['dayNameLength']  How long the day names header should be.
	 *                               0 for none
	 *                               1-3 for that length
	 *                               > 3 will just display the whole name 🤷
	 *
	 */
	function __construct( $date=null, array $args=[] ) {
		foreach ( $args as $prop => $arg ) {
			switch ( $prop ) {
				case 'dayNameLength':
					$this->setDayNameDisplayNameLength( $arg );
					break;
			}
		}

		if ( ! ( $date instanceof DateTime ) ) {
			$this->thisMonth = new DateTime( $date );
		} else {
			$this->thisMonth = $date;
		}

		if ( $this->thisMonth->format( 'j' ) != '1' ) {
			$this->thisMonth->setDate(
				$this->thisMonth->format( 'Y' ),
				$this->thisMonth->format( 'm' ),
				1
			);
		}

		$this->today = date( 'Y-m-d' );

		$this->nextMonth = clone $this->thisMonth;
		$this->nextMonth->modify( '+1 month' );

		$this->prevMonth = clone $this->thisMonth;
		$this->prevMonth->modify( '-1 month' );

		// echo '<pre>---$this->thisMonth---<br>';var_dump($this->thisMonth);echo'</pre>';
		// echo '<pre>---$this->nextMonth---<br>';var_dump($this->nextMonth);echo'</pre>';
		// echo '<pre>---$this->prevMonth---<br>';var_dump($this->prevMonth);echo'</pre>';

		// @NOTE Wordpress library functions
		$this->linkOriginal = esc_url( remove_query_arg( [ 'jpme_date' ] ) );
		// @NOTE Wordpress library functions
		$this->linkNextMonth = esc_url(
			add_query_arg( 'jpme_date', $this->nextMonth->format( 'Y-m' ) )
		);
		// @NOTE Wordpress library functions
		$this->linkPrevMonth = esc_url(
			add_query_arg( 'jpme_date', $this->prevMonth->format( 'Y-m' ) )
		);

		// default to EST, but use timezone in WP settings if set
		// $this->timezone = get_option( 'timezone_string', 'America/New_York' );
	}

	function add_events ( array $events ) {
		$this->events = array_merge(
			$this->events,
			$events
		);
	}

	function setDayNameDisplayNameLength ( int $len ) {
		$this->dayNameDisplayLength = $len;
	}

	private function isCurrentMonth () {
		return ( date( 'Y-m', strtotime( $this->today ) ) == $this->thisMonth->format( 'Y-m' ) );
	}

	// @TODO rewrite this using more flexible date objects
	function render () {

		// $this->prevMonth = str_pad( $this->prevMonth, 2, '0', STR_PAD_LEFT);
		// $this->nextMonth = str_pad( $this->nextMonth, 2, '0', STR_PAD_LEFT);

		// $first_of_month = gmmktime(0, 0, 0, $this->month, 1, $this->year );
		$first_of_month = $this->thisMonth->getTimestamp();

		$weekday = $this->thisMonth->format( 'N' );

		$weekday = ( $weekday + 7 - self::FIRST_DAY ) % 7; // adjust for first day

		$title = $this->thisMonth->format( 'F' ) . ' ' . $this->thisMonth->format( 'Y' );

		$calendar = '<div id="jpme-cal-grid" class="jpme-cal-grid">';

		$calendar .= apply_filters( 'jpme_cal_grid_top', '' );

		$returnLink = $this->isCurrentMonth()
			? ''
			: '<a class="jpme-cal-returnToToday jpme-button-link" href="' . $this->linkOriginal . '">Go to Today</a>';

		$calendar .= <<<HTML
			<table class="jpme-cal-grid_calendar" id="event-calendar-view">

				<caption class="jpme-cal-grid_title"> 
					<h2 class="jpme-cal-grid_monthName">{$title}</h2>
					
					<div class='jpme-cal-grid_nav'>
						<a class='jpme-cal-grid_navPrev jpme-button-link' href="{$this->linkPrevMonth}#event-calendar-view">
							&laquo; Prev
						</a>
						{$returnLink}
						<a class='jpme-cal-grid_navNext jpme-button-link' href="{$this->linkNextMonth}#event-calendar-view">
							Next &raquo;
						</a>
					</div>
				</caption>				
		HTML;

		if ( $this->dayNameDisplayLength ) {
			$calendar .= '<thead>
				<tr>';

			foreach ( self::DAY_NAMES as $d ) {
				// @TODO clean this up
				$calendar .= '<th>';
				$calendar .= ( $this->dayNameDisplayLength < 4 )
					? substr( $d, 0, $this->dayNameDisplayLength )
					: $d;
				$calendar .= '</th>';
			}

			$calendar .= '</tr>
				</thead>';
		}

		$calendar .= '<tbody>
			<tr>';

		// initial empty days
		if ( $weekday > 0 ) {
			$calendar .= '<td class="jpme-cal-grid_emptycells" colspan="' . $weekday . '">&nbsp;</td>';
		}

		// day box loop
		for ( $day = 1, $days_in_month = $this->thisMonth->format( 't' );
			$day <= $days_in_month;
			$day++, $weekday++
		) {
			$dayTwoDigit = str_pad( $day, 2, '0', STR_PAD_LEFT );

			$dateString = $this->thisMonth->format( 'Y' ) . '-' . $this->thisMonth->format( 'm' ) . '-' . $dayTwoDigit;

			if ( $weekday == 7 ) {
				$weekday = 0; // start new week row
				$calendar .= '</tr><tr>';
			}

			$current = ( $dateString == $this->today )
				? ' class="jpme-cal-grid_currentDay" '
				: '';

			$calendar .= "<td {$current}>
				<span class='jpme-cal-grid-day'>{$day}</span>";

			// if there are any events on this date
			if ( isset( $this->events[ $dateString ] ) ) {

				foreach ( $this->events[ $dateString ] as $daysEvents ) {
					foreach ( $daysEvents as $ev ) :

						$title = $ev->getTitle();
						$time = $ev->getEventTime();
						if ( $time )
							$title .= ' &mdash; ' . $time;
						$link = $ev->getLink();

						$tags = $ev->getEventTags( 'list', 'slug', ' ' );
						$tags = esc_attr( $tags );

						$calendar .= "<a href='{$link}' class='jpme-cal-grid_item' data-tags='{$tags}'>{$title}</a>";

					endforeach;
				}
			}

			$calendar .= '</td>';
		}

		// empty days at end of calendar
		if ( $weekday != 7 ) {
			$calendar .= '<td class="jp-cal-grid_emptyCells" colspan="' . ( 7 - $weekday ) . '">&nbsp;</td>'; // empty days
		}

		$calendar .= '</tr>
			</tbody>
		 	</table>';

		$calendar .= apply_filters( 'jpme_cal_grid_bottom', '' );

		$calendar .= '</div>';

		return $calendar;
	}
}
