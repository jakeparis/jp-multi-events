<?php
if( class_exists('Jp_Cpt_Metabox') )
	return;


class Jp_Cpt_Metabox {
	public string $boxId;
	public string $boxTitle;
	public bool $showInBlockEditor = true;
	public $on_render_callback;
	public $on_save_callback;

	private string $boxLocation = "advanced";
	private string $boxPriority = "default";
	private string $postType;
	private string $nonceName;
	private string $nonceAction;
	// private string $nonce;

	function __construct( string $postType, string $boxId, string $boxTitle ) {

		$this->boxId = $boxId;
		$this->boxTitle = $boxTitle;
		$this->postType = $postType;

		add_action("add_meta_boxes_{$postType}", [ $this, 'do_add' ] );
		add_action("save_post_{$postType}", [ $this, 'do_save' ] );

		$this->nonceName = "_nonce_{$postType}_{$boxId}";
		$this->nonceAction = "save_meta_{$postType}_{$boxId}";
	}
	
	function setLocation ( string $loc ) {
		if( in_array($loc, ['side', 'normal', 'advanced']) )
			$this->boxLocation = $loc;
	}
	
	function setPriority ( string $priority ) {
		if( in_array($priority, ['low', 'core', 'default', 'high']) )
			$this->boxPriority = $priority;
	}

	function showInBlockEditor( bool $show ) {
		$this->showInBlockEditor = $show;
	}

	function setTitle( string $t ) {
		$this->boxTitle = $t;
	}
	
	/**
	 * Render (or return) the nonce hidden input field
	 *
	 * @param bool $echo Whether to echo or only return the field
	 *
	 * @return string The html hidden input field
	 */
	function renderNonce ( bool $echo=true ) {
		return wp_nonce_field( $this->nonceAction, $this->nonceName, true, $echo );
	}

	/**
	 * Verify the nonce created with $this->renderNonce()
	 *
	 * @return bool True if verified, false if unable to verify
	 */
	function verifyNonce () {
		if( ! isset($_POST[$this->nonceName]) )
			return false;
		return wp_verify_nonce($_POST[$this->nonceName], $this->nonceAction);
	}

	/**
	 * Set how the metabox renders
	 *
	 * @param callable $func Give a function to render. 
	 * 					First parameter will be post object .
	 *
	 * @return void
	 */
	function onRender( callable $func ) {
		$this->on_render_callback = $func;
	}

	function onSave ( callable $callback ) {
		$this->on_save_callback = $callback;
	}

	function do_add ( $current_post_type ) {

		$args = [];
		
		if( ! $this->showInBlockEditor )
			$args['__back_compat_meta_box'] = true;

		add_meta_box( $this->boxId, $this->boxTitle, [ $this, 'do_render'], $this->postType, $this->boxLocation, $this->boxPriority );
	}

	function do_render ($post) {
		$this->renderNonce();
		if( is_callable($this->on_render_callback) )
			call_user_func( $this->on_render_callback, $post );

	}

	function do_save ( $post_id ) {

		if ( ! current_user_can( 'edit_post', $post_id ) )
			return;
		if ( wp_is_post_autosave( $post_id ) )
			return;
		if ( wp_is_post_revision( $post_id ) )
			return;
		if ( ! $this->showInBlockEditor && wp_is_json_request() )
			return;

		if( $this->verifyNonce() === false) {
			error_log('Unable to verify nonce for ' . $this->boxTitle);
			return false;
		}

		call_user_func( $this->on_save_callback, $post_id );
	}

}