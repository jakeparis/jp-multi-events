<?php
if( class_exists('Jp_Plugin_Blocks_Manager') )
	return;

class Jp_Plugin_Blocks_Manager {
	protected $pluginSlug;
	protected $version;

	protected $registeredBlocks = [];

	private function __construct ( string $pluginSlug, string $version ) {
		$this->pluginSlug = $pluginSlug;
		$this->version = $version;
	}

	static function getManager ( string $pluginSlug, string $version ) : Jp_Plugin_Blocks_Manager {
		static $managers = [];
		$key = $pluginSlug;
		if( ! isset($managers[$key]) )
			$managers[$key] = new self( $pluginSlug, $version );
		
		return $managers[$key];
	}


	/**
	 * Auto-register all blocks with a block.json file and call any registration.php
	 * files in the built directories
	 * 
	 * Checks the given $path for subdirectories; looks in each for a block.json and a
	 * registration.php.  If a registration.php is found, it is run. If a block.json is found,
	 * it is used to enqueue the block. If block.json exists, and registration.php had a return 
	 * value, it is used as an $args array to feed into register_block_type() function.
	 * 
	 * You can add any block-associated php script needs into the registration.php. If an array 
	 * is returned from that file, it will be used as the second argument to 
	 * register_block_type. Mostly likely this array would have a "render_callback" 
	 * key, and perhaps an "attributes" key.
	 * 
	 * WordPress will automatically enqueue the necessary scripts and css based on the 
	 * "editorScript", etc., fields in the block.json file
	 * 
	 * @param string $path the relative path to the build directory with the block directories
	 *                     starting from the plugin root -- no slash at beginning, no slash at 
	 *                     end. Defaults to "build/blocks". Expects this directory to contain
	 *                     a bunch of directories, one for each block containing a block.json
	 *
	 * @return void
	 */
	public function register_blocks ( string $path="build/blocks" ) {
		add_action('init', function() use ($path) {
			$blockPath = WP_PLUGIN_DIR . '/' . trim($this->pluginSlug,'/') . '/' . trim($path,'/');

			foreach ( glob( $blockPath . '/*/' ) as $blockdir ) {
				$args = [];
				if( file_exists($blockdir . "registration.php") ) {
					$args = require $blockdir . "registration.php";
				}
				if( file_exists($blockdir . "block.json") ) {
					if( ! is_array($args) )
						$args = array();
					$this->registeredBlocks[] = register_block_type( $blockdir . "block.json" , $args );
				}
			}
			// remove any empty values
			$this->registeredBlocks = array_filter($this->registeredBlocks);
		}, 15);
	}

	/**
	 * Get the WP_Block_Type definition for a one of the blocks registered by register_blocks()
	 *
	 * @param string $blockName The name of the block
	 *
	 * @return WP_Block_Type|false
	 */
	public function getRegisteredBlock ( string $blockName ) {
		if( empty($this->registeredBlocks) )
			return false;
		foreach($this->registeredBlocks as $BlockTypeDef) {
			if( $BlockTypeDef->name === $blockName )
				return $BlockTypeDef;
		}
		return false;
	}

	/**
	 * Get the handle for a style/script registered by register_blocks(). For example, "viewScript" or "editorStyle" 
	 *
	 * @param string $blockName  the slug of the block 
	 * @param string $assetName  The name of the assets as defined in block.json
	 *
	 * @return array|false
	 */
	public function getRegisteredAssetNames( string $blockName, string $assetName ) {
		include_once ABSPATH . WPINC . '/version.php';
		global $wp_version;

		$map = [
			'editorScript' => 'editor_script',
			'editorStyle' => 'editor_style',
			'viewScript' => 'view_script',
			'script' => 'script',
			'style' => 'style',
		];
		if( ! isset($map[$assetName]) ) {
			error_log('Bad name used for getRegisteredAssetNames(): ' . $assetName);
			return false;
		}

		$block = $this->getRegisteredBlock($blockName);
		if ( ! $block )
			return false;

		if( version_compare( $wp_version, '6.1.0' ) < 0 ) {
			// pre WP 6.1
			$assetName = $map[$assetName];
			if( property_exists( $block, $assetName ) ) {
				return array(
					$block->{$assetName}
				);
			}
		} else {
			$assetName = $map[$assetName] . "_handles";
			if( property_exists( $block, $assetName) ) {
				return $block->{$assetName};
			}
		}
		
		return false;
	}

	/**
	 * Enqueue the public javascript file for a block registered with register_blocks
	 *
	 * @param string $blockName  
	 *
	 * @return void
	 */
	public function enqueuePublicJs ( string $blockName ) {
		$handles = $this->getRegisteredAssetNames( $blockName, 'viewScript' );
		if( $handles ) {
			foreach($handles as $handle) { 
				wp_enqueue_script( $handle );
			}
		} else
			error_log('File requested to enqueue doesn\'t exist: ' . $blockName . '/viewScript');
	}

	/**
	 * If any editor blocks or scripts don't have a block.json file (for example, a sidebar plugin), they will
	 * need to be registered by calling this function. 
	 *
	 * @param string  $path the relative path where the main js file is, starting from the 
	 *                      plugin root -- no slash at beginning, no slash at end. Defaults to
	 *                      "build". Used for both filesystem path and url
	 * @param string  $filename  The name of the entry js file. Defaults to "index.js" Same name will be used to 
	 *                           look for a css file. For example, if "foobar.js" is passed, a foobar.css will 
	 *                           be enqueued if it exists.
	 * @param string  $inlineData  A string of inline css to attach to 
	 * 	                          the given item (no <style> tag).
	 *                            See wp_add_inline_style() for more information
	 * 
	 * @return void
	 */
	public function register_editor_assets ( string $path="build", string $filename="index.js", string $inlineData="" ) {
		if( $inlineData ){
			$inlineData['data'] = $inlineData;
		}
		return $this->register_built_assets( $path, 'editor', $filename, [
			'inlineData' => $inlineData
		] );
	}

	/**
	 * Any built public scripts/styles can be enqueued by calling this function. 
	 *
	 * @param string  $path the relative path where the main js file is, starting from the 
	 *                      plugin root -- no slash at beginning, no slash at end. Defaults to
	 *                      "build". Used for both filesystem path and url
	 * @param string  $filename  The name of the entry js file. Defaults to "public.js" Same name will be used to 
	 *                           look for a css file. For example, if "foobar.js" is passed, a foobar.css will 
	 *                           be enqueued if it exists.
	 * @param array  $inlineData  An array with two items, representing inline javascript to attach to 
	 * 	                           the given item. The array must have at least the "data" key, which is 
	 *                             a string of js (no <script> tag). An additional key for
	 *                             "position" can be passed with either "before" or "after" (defaults to "after")
	 *                             See wp_add_inline_script() for more information
	 * 
	 * @return void
	 */
	public function register_public_assets ( string $path="build", string $filename="public.js", array $inlineData=[] ) {
		return $this->register_built_assets( $path, 'public', $filename, [
			'inlineData' => $inlineData,
		] );
	}

	/**
	 * A low level function to enqueue scripts and styles created by the build script, for either front end or editor.
	 * 
	 * You should probably use register_public_assets() or register_editor_assets() instead.
	 *
	 * @param string $path      the relative path where the main js file is, starting from the 
	 *                          plugin root -- no slash at beginning, no slash at end. Defaults to
	 *                          "build". Used for both filesystem path and url
	 * @param string $flag      Type of file being enqueued. Accepts either "editor" or "public"
	 * @param string $filename  The name of the entry js file. if $flag=='editor', defaults to 'index.js'. If $flag=='public'
	 *                          defaults to 'public.js'
	 * @param array  $options   An array of options:
	 * @param mixed $options['inlineData']  An array representing inline js/css to attach to 
	 * 	                        the given item. The array must have at least the "data" key, which is 
	 *                          a string of css/js (no script/style tags). For javascript, an additional key for
	 *                          "position" can be passed with either "before" or "after" (defaults to "after")
	 *                          See wp_add_inline_script() or wp_add_inline_style() for more information
	 *
	 * @return void
	 */
	public function register_built_assets ( string $path="build", string $flag="editor", string $filename="", array $options=[] ) {
		switch ($flag) {
			case 'editor' :
				$hook = 'enqueue_block_editor_assets';
				$filename = empty($filename) ? 'index.js' : $filename;
				break;
			case 'public' :
				// if we've enqueued a script inside the block/shortcode, we'll need to use a
				// later hook
				$hook = did_action('wp_enqueue_scripts') ? 'wp_footer' : 'wp_enqueue_scripts';
				$filename = empty($filename) ? 'public.js' : $filename;
				break;
			default :
				error_log('Incorrect flag passed to register_built_assets');
				return false;
		}

		$path = trim($path, '/' );
		add_action( $hook, function () use ($path, $filename,$flag, $options) {
			$dir = WP_PLUGIN_DIR . '/' . $this->pluginSlug . "/" . $path . "/";
			if( str_contains($filename, '.' ) )
				list($fileRoot) = explode('.',$filename);

			if( file_exists( $dir . $fileRoot . '.js' ) ) {
				if( file_exists( $dir . $fileRoot . ".asset.php") ) {
					list('version'=>$version,'dependencies'=>$deps) = include $dir . $fileRoot . ".asset.php";
				} else {
					$deps = [];
					$version = $this->version;
				}

				/**	
				 * Filter: jp_plugin_core_{$pluginSlug}_block_{$flag}_script_dependencies
				 * 
				 * Add dependencies for a manually enqueued built script file. $flag will be either "public" or "editor"
				 */
				$deps = apply_filters("jp_plugin_core_{$this->pluginSlug}_block_{$flag}_script_dependencies", $deps );

				$handle = $this->pluginSlug . "-{$flag}-script";
				wp_enqueue_script( $handle, 
					plugins_url( $this->pluginSlug . "/{$path}/{$fileRoot}.js" ),
					$deps,
					$version
				);
				if( isset($options['inlineData']) && ! empty($options['inlineData']['data']) ) :
					$dataObj = $options['inlineData']; 
					$pos = isset($dataObj['position']) ? $dataObj['position'] : 'after';
					wp_add_inline_script($handle, $dataObj['data'], $pos );
				endif;
			}

			if( file_exists($dir . $fileRoot . ".css") ) {
				$handle = $this->pluginSlug . "-{$flag}-style";
				wp_enqueue_style( $handle, 
					plugins_url( $this->pluginSlug . "/{$path}/{$fileRoot}.css" ),
					[],
					$version
				);
				if( isset($options['inlineData']) && ! empty($options['inlineData']['data']) ) :
					$dataObj = $options['inlineData']; 
					wp_add_inline_style($handle, $dataObj['data']);
				endif;
			}
		});
	}
}