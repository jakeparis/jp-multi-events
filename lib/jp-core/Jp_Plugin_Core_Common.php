<?php
if( trait_exists('Jp_Plugin_Core_Common') )
	return;

trait Jp_Plugin_Core_Common {

	/**
	 * Get the options manager singleton, keyed by the plugin slug
	 *
	 * @return Jp_Plugin_Options_Manager
	 */
	public function get_options_manager () : Jp_Plugin_Options_Manager {
		require_once 'Jp_Plugin_Options_Manager.php';
		return Jp_Plugin_Options_Manager::getManager( $this->pluginSlug );
	}
	
	/**
	 * Get the network-options manager singleton, keyed by the plugin slug
	 *
	 * @return Jp_Plugin_Options_Manager
	 */
	public function get_network_options_manager () : Jp_Plugin_Options_Manager {
		require_once 'Jp_Plugin_Options_Manager.php';
		return Jp_Plugin_Options_Manager::getManager( $this->pluginSlug, "network" );
	}

	/**
	 * Get the Blocks Manager for this plugin
	 *
	 * @return Jp_Plugin_Blocks_Manager
	 */
	public function get_blocks_manager () : Jp_Plugin_Blocks_Manager {
		require_once 'Jp_Plugin_Blocks_Manager.php';
		return Jp_Plugin_Blocks_Manager::getManager( $this->pluginSlug, $this->version );
	}

}