<?php
if( class_exists('Jp_Cpt_Core') )
	return;

/**
 * Normal use might look like
 * 
 * ```
 * $myCpt = new Jp_Cpt_Core( 'my-cpt', $myPostTypeRegistrationArgs );
 * $myCpt->add_taxonomy( 'colors', $myTaxonomyRegistrationArgs );
 * // optional fourth param for a sortable column
 * $myCpt->add_admin_column( 'colors', 'Custom Colors', /render callback goes here/ );
 * 
 * $manualMeta = myCpt->add_meta_box( 'manual-meta', 'Manual Meta' );
 * $manualMeta->onRender( function($Post) {
 * 	echo 'I am here';
 * }) );
 * $manualMeta->onSave( function($post_id) {
 * 	update_post_meta()... etc
 * });
 */
class Jp_Cpt_Core {
	protected string $post_type;
	private array $post_type_args = [];
	private array $adminColumns = [];
	private array $taxonomies = [];
	private $post_type_object = null;

	/**
	 * Set up a new cpt. You can also pass the slug for an existing object to use this
	 * as a wrapper around that
	 *
	 * @param string $post_type
	 * @param array (optional) $post_type_args
	 */
	function __construct ( string $post_type, array $post_type_args=[] ) {
		$this->post_type = $post_type;

		if( count($post_type_args) )
			$this->set_posttype_args($post_type_args);

		// if( post_type_exists($post_type) ) {
		// 	$this->post_type_object = get_post_type_object($post_type);
		// 	return;
		// }

		$this->init_post_type();
	}

	function set_posttype_args ( array $post_type_args ) {
		$this->post_type_args = $post_type_args;
	}

	/**
	 * Add a taxonomy to this post type
	 *
	 * @param string $termslug
	 * @param array $args
	 *
	 * @return void
	 */
	function add_taxonomy ( string $termslug, array $args ) {
		$this->taxonomies[$termslug] = $args;
	}

	private function init_post_type () {
		add_action('init', [ $this, 'register' ] );

		add_filter("manage_{$this->post_type}_posts_columns", [ $this, 'do_custom_columns'] );
		// This hook only fires if the current post type is non-hierarchical, such as posts.
		add_action("manage_{$this->post_type}_posts_custom_column", [$this, 'render_custom_column'], 10, 2);

		add_action("manage_edit-{$this->post_type}_sortable_columns", [ $this, 'do_sortable_columns'] );
	}

	function register () {
		if( post_type_exists($this->post_type) )
			return;
		if( $this->taxonomies ) {
			$this->post_type_args['taxonomies'] = array_merge(
				$this->post_type_args['taxonomies'],
				array_keys($this->taxonomies)
			);
		}
		$this->post_type_object = register_post_type( $this->post_type, $this->post_type_args );
		$this->register_taxonomies();
	}

	function register_taxonomies () {
		foreach( $this->taxonomies as $termslug => $args ) {
			register_taxonomy( $termslug, $this->post_type, $args );
		}
	}

	function get_post_type_object () {
		return $this->post_type_object;
	}

	/**
	 * Add a meta box for this post type
	 *
	 * @param string $boxId
	 * @param string $boxTitle
	 * @param string $boxLocation  (optional) side, normal, or advanced (the default)
	 * @param string $boxPriority  (optional) low, core, default (the default), or high
	 * @param bool $showInBlockEditor  (optional) pass false to only show in classic editor
	 *
	 * @return Jp_Cpt_Metabox
	 */
	public function add_meta_box (
		string $boxId,
		string $boxTitle,
		string $boxLocation="advanced",
		string $boxPriority="default",
		bool $showInBlockEditor=true
	) : Jp_Cpt_Metabox {
		require_once 'Jp_Cpt_Metabox.php';

		$metabox = new Jp_Cpt_Metabox( $this->post_type, $boxId, $boxTitle);
		$metabox->setLocation( $boxLocation );
		$metabox->setPriority( $boxPriority );
		$metabox->showInBlockEditor($showInBlockEditor);
		
		return $metabox;
	}


	/**
	 * Add an admin column for this post type
	 *
	 * @param string $slug	The unique slug for this column
	 * @param string $title	 The column heading
	 * @param callback $callback	What to render for this row in this column
	 * @param callback $sortableMethod	(optional) provide a filter to use for $query_vars in order to sort by this column
	 *
	 * @return void
	 */
	public function add_admin_column( string $slug, string $title, callable $callback, $sortableMethod=null ) {
		$this->adminColumns[ $slug ] = [
			'title' => $title,
			'callback' => $callback,
			'sortableCallback' => $sortableMethod
		];
	}

	/**
	 * Remove an admin column for this post type
	 *
	 * @param string $slug
	 *
	 * @return void
	 */
	function remove_admin_column ( string $slug ) {
		add_filter("manage_{$this->post_type}_posts_columns", function($cols) use ($slug) {
			unset( $cols[$slug] );
			return $cols;
		}, 20);
	}

	function do_custom_columns ($cols) {
		foreach( $this->adminColumns as $slug=>$data) {
			$cols[$slug] = $data['title'];
		}
		return $cols;		
	}

	function render_custom_column ($col, $post_id) {
		if( empty($this->adminColumns) )
			return "";
		if( ! array_key_exists($col, $this->adminColumns) )
			return "";

		call_user_func( $this->adminColumns[$col]['callback'], $post_id );
	}

	function do_sortable_columns ($cols) {
		$sortableCols = array_filter($this->adminColumns, function($col) {
			return $col['sortableCallback'] != null;
		});
		if( empty($sortableCols) )
			return $cols;

		foreach($sortableCols as $slug=>$data){
			$cols[$slug] = $slug;
			add_action('request', function($vars) use ($slug,$data) {
				if( ! is_admin() || $vars['post_type'] != $this->postType )
					return;
				if( $vars['orderby'] == $slug )
					$vars = $data['sortableCallback']( $vars );
			});
		}

		return $cols;
	}

}
