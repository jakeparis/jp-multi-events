<?php
if( class_exists('Jp_Plugin_Options_Manager') )
	return;

class Jp_Plugin_Options_Manager {
	
	protected $pluginSlug;
	protected $optionName;
	protected $storageLocation;

	private $settings = null;
	private $defaults = [];
	

	private function __construct ( string $pluginSlug, string $optionPostfix = "_options", string $storageLocation = "site" ) {
		$this->pluginSlug = $pluginSlug;
		$this->optionPostfix = "{$pluginSlug}_{$optionPostfix}";

		$this->storageLocation = ($storageLocation === 'network' ) ? 'network' : 'site';
	}

	public function setDefaults ( array $defaults ) {
		$this->defaults = $defaults;
		$this->load(true);
	}

	public function getDefault ( string $key ) {
		if( isset($this->defaults[$key]) )
			return $this->defaults[$key];
		return false;
	}

	public function useNetworkStorage () {
		$this->storageLocation = "network";
	}

	public function useSiteStorage () {
		$this->storageLocation = 'site';
	}

	public function getStorageLocation () {
		return $this->storageLocation;
	}

	protected function load ( bool $overrideCache=false ) {
		if( $this->settings === null || $overrideCache == true ) {
			if( $this->storageLocation === 'network' )
				$this->settings = get_site_option( $this->optionName, $this->defaults );
			else
				$this->settings = get_option( $this->optionName, $this->defaults );
		}
	}

	public function getAll( bool $overrideCache=false ) {
		$this->load($overrideCache);
		return $this->settings;
	}

	public function get ( string $key, $default=false ) {
		$this->load();
		// even if empty, current value is used if set rather than default
		if( isset($this->settings[ $key ]) )
			return $this->settings[ $key ];
		if( $default )
			return $default;
		return $this->getDefault( $key ); 
	}

	public function save ( $keyOrPairs, $value=null ) {
		if( is_string($keyOrPairs) )
			$this->settings[$keyOrPairs] = $value;
		else
			$this->settings = $keyOrPairs;

		if( $this->storageLocation === 'network' )
			return update_site_option( $this->optionName, $this->settings );
		
		return update_option( $this->optionName, $this->settings );
	}

	public function update( $keyOrPairs, $value = null ) {
		return $this->save( $keyOrPairs, $value );
	}

	public function delete ( string $key ) {
		if( ! array_key_exists($key, $this->settings) )
			return false;
		
		$old = $this->settings[$key];
		unset( $this->settings[$key] );
		$this->save( $this->settings );
		return $old;
	}

	public function deleteAll () {
		$this->settings = null;
		if( $this->storageLocation === 'network' )
			return delete_site_option( $this->optionName );
		else
			return delete_option( $this->optionName );
	}


	static function getManager ( string $pluginSlug, string $storage="site", string $optionPostfix="_options" ) : Jp_Plugin_Options_Manager {
		static $managers = [];
		$key = $pluginSlug . '|' . $storage . '|' . $optionPostfix;
		if( isset( $managers[$key] ) )
			return $managers[$key];
		$managers[$key] = new self($pluginSlug, $optionPostfix, $storage);
		return $managers[$key];
	}
}