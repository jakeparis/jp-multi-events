<?php

class Jp_Multi_EventCollection {

	private int $limit = 20;
	private string $limitToFutureOrPast = '';
	private string $tagLimit = '';
	private bool $groupEvents = true;
	private string $sortOrder = 'ASC';
	private array $hiddenEventParts = [];

	protected $startDate = null;
	protected $endDate = null;
	protected $events = [];
	protected $orderedEvents = [];
	protected $has_next_page = false;
	protected $has_prev_page = false;
	protected $current_page = 1;

	public function __construct( array $args=[] ) {

		foreach ( $args as $propName => $arg ) {

			switch ( $propName ) {
				case 'onlyPast':
					if ( $arg )
						$this->setToPastOnly();
					break;
				// @NOTE onlyFuture overrides onlyPast
				case 'onlyFuture':
					if ( $arg )
						$this->setToFutureOnly();
					break;
				case 'limit':
					$this->setLimit( $arg );
					break;
				case 'tag':
				case 'tags':
				case 'tagLimit':
				case 'onlyTag':
				case 'showTag':
				case 'event-tag':
					$this->setOnlyShowTags( $arg );
					break;
				case 'group-events':
					$this->setGroupOccassions( (bool) $arg );
					break;
				case 'order':
					$this->setSortOrder( $arg );
					break;
				case 'startDate':
					$this->setStartDate( $arg );
					break;
				case 'endDate':
					$this->setEndDate( $arg );
					break;
			}
		}
	}

	public function setLimit ( int $limit ) {
		$this->limit = $limit;
	}
	public function setToFutureOnly () {
		$this->limitToFutureOrPast = 'future';
		$this->setSortOrder( 'ASC' );
	}
	public function setToPastOnly () {
		$this->limitToFutureOrPast = 'past';
		$this->setSortOrder( 'DESC' );
	}
	public function setOnlyShowTags ( string $tag ) {
		$this->tagLimit = $tag;
	}
	public function setGroupOccassions ( bool $toGroup ) {
		$this->groupEvents = $toGroup;
	}
	public function setSortOrder ( string $order ) {
		$this->sortOrder = ( strtolower( $order ) === 'asc' ) ? 'ASC' : 'DESC';
	}
	public function setHiddenEventParts ( $parts ) {
		if ( is_string( $parts ) )
			$parts = explode( ',', $parts );
		array_walk( $parts, 'trim' );
		$this->hiddenEventParts = $parts;
	}
	public function setStartDate ( $date ) {
		if ( is_string( $date ) )
			$date = strtotime( $date );
		$this->startDate = $date;
	}
	public function setEndDate ( $date ) {
		if ( is_string( $date ) )
			$date = strtotime( $date );
		$this->endDate = $date;
	}

	/**
	 * Here we use wordpress sql to get as few event posts as possible meeting
	 * the criteria. Then wrap each Post as a Jp_Multi_Event
	 *
	 * @return void
	 */
	public function load_events () {
		$postArgs = array(
			'post_type' => 'event',
			'nopaging' => true, // we'll custom page later
			'orderby' => 'meta_value',
			'order' => $this->sortOrder,
			'meta_query' => [
				[
					'key' => '_Event Date',
				],
			],
		);

		// if ( $this->limit < 0 ) {
		// 	$postArgs['nopaging'] = true;
		// } else {
		// 	$postArgs['posts_per_page'] = $this->limit;
		// 	$postArgs['paged'] = ( get_query_var( 'paged' ) )
		// 		? get_query_var( 'paged' )
		// 		: 1;
		// }

		if ( ! empty( $this->tagLimit ) ) {
			$postArgs['tax_query'] = array(
				array(
					'taxonomy' => 'event-tag',
					'field' => 'slug',
					'terms' => $this->tagLimit,
				),
			);
		}

		// this will get events with dates in the future, but ALSO get events
		// with a date in the future AND a date in the past. So it at least limits some
		// traffic, but not perfect results.
		$startDate = $endDate = false;
		// @NOTE startDate and endDate override limitToFutureOrPast values

		if ( $this->startDate )
			$startDate = $this->startDate;
		// limit-to-future means the same as startDate=now
		elseif ( $this->limitToFutureOrPast === 'future' )
			$startDate = time();

		if ( $this->endDate )
			$endDate = $this->endDate;
		// only-get-past means same as endDate=now
		elseif ( $this->limitToFutureOrPast === 'past' )
			$endDate = time();

		if ( $startDate && $endDate ) {

			$postArgs['meta_query'][0]['value'] = [
				date( 'Y-m-d', $startDate ),
				date( 'Y-m-d', $endDate ),
			];
			$postArgs['meta_query'][0]['compare'] = 'BETWEEN';
			$postArgs['meta_query'][0]['type'] = 'DATE';

		} elseif ( $startDate ) {

			$postArgs['meta_query'][0]['value'] = date( 'Y-m-d', $startDate );
			$postArgs['meta_query'][0]['compare'] = '>=';
			$postArgs['meta_query'][0]['type'] = 'DATE';

		} elseif ( $endDate ) {

			$postArgs['meta_query'][0]['value'] = date( 'Y-m-d', $endDate );
			$postArgs['meta_query'][0]['compare'] = '<';
			$postArgs['meta_query'][0]['type'] = 'DATE';

		}

		$posts = get_posts( $postArgs );

		if ( ! $posts )
			return false;

		array_walk( $posts, function ( &$post ) {
			$post = new Jp_Multi_Event( $post );
		});

		$this->events = $posts;
	}

	public function count_events () {
		return count( $this->events );
	}

	/**
	 * Here we use php to sort the events, and filter some more out based on our
	 * criteria (things that couldn't be sorted out in the wordpress sql)
	 * 
	 * We'll also implement our own paging/counting here
	 *
	 * @return void
	 */
	public function sort_events () {
		if ( ! $this->events )
			return;

		// Don't bother if we've already done this
		if ( count( $this->orderedEvents ) )
			return;

		$today = date( 'Y-m-d' );
		$stream = [];

		foreach ( $this->events as $ev ) {
			// Even though we attempted to limit events in the sql query, some occurences
			// get through because they have one occurence out of bounds and one in. So
			// we still have to do some checks
			foreach ( $ev->getEventDates( 'Y-m-d' ) as $date ) {
				if ( $this->limitToFutureOrPast === 'future' && $date < $today )
					continue;
				if ( $this->limitToFutureOrPast === 'past' && $date >= $today )
					continue;
				if ( $this->startDate && date( 'Y-m-d', $this->startDate ) > $date )
					continue;
				if ( $this->endDate && date( 'Y-m-d', $this->endDate ) <= $date )
					continue;
				if ( ! isset( $stream[ $date ] ) )
					$stream[ $date ] = array();

				// if an array key is FALSE, it's the same as doing $array[] = $ev;
				$time = $ev->getEventTime( 'Gi' );
				if ( empty( $time ) )
					$time = '0000';

				if ( ! isset( $stream[ $date ][ $time ] ) )
					$stream[ $date ][ $time ] = array();

				$stream[ $date ][ $time ][] = $ev;
				$eventSeen = true;
			}
		}

		if ( $this->sortOrder === 'DESC' ) {
			krsort( $stream );
			foreach ($stream as $k => $v)
				krsort( $stream[ $k ] );
		} else {
			ksort( $stream );
			foreach ($stream as $k => $v)
				ksort( $stream[ $k ] );
		}

		// Implement our own paging
		$per_page = ( $this->limit < 0 ) ? PHP_INT_MAX : $this->limit;
		$this->current_page = empty( $_GET['evpg'] ) ? 1 : (int) $_GET['evpg'];
		$page_begin = ( $this->current_page - 1 ) * $per_page;
		$page_end = $page_begin + $per_page;
		$out = [];

		if ( $this->current_page > 1 )
			$this->has_prev_page = true;

		$event_index = 0;
		$event_ids = [];
		foreach ( $stream as $date => $timeSet ) {
			foreach ( $timeSet as $time => $events ) {
				foreach ( $events as $event_list_key => $event ) {
					if ( $event_index >= $page_end ) {
						// check if we have more events
						if (
							array_key_last( $events ) != $event_list_key
							||
							array_key_last( $timeSet ) != $time
							||
							array_key_last( $stream ) != $date
						) {
							// we have more events
							$this->has_next_page = true;
						}
						break 3;
					}
					// skip all events until we get to our page
					if ( $event_index < $page_begin ) {
						// only count events towards paging if they aren't marked
						// as "group in lists" (or we haven't seen that one yet)
						if (
							! in_array( $event->ID, $event_ids )
							||
							! $this->groupEvents
							||
							! $event->shouldGroupOccurences()
						) {
							$event_index++;
						}
						$event_ids[] = $event->ID;
						continue;
					}
					if (
						$this->groupEvents
						&&
						$event->shouldGroupOccurences()
						&&
						in_array( $event->ID, $event_ids )
					) {
						// do NOT set $event_index++ here, so we don't count grouped
						// events towards paging
						continue;
					}
					if ( ! array_key_exists( $date, $out ) ) {
						$out[ $date ] = array();
					}
					if ( ! array_key_exists( $time, $out[ $date ] ) ) {
						$out[ $date ][ $time ] = array();
					}
					$out[ $date ][ $time ][] = $event;
					$event_ids[] = $event->ID;
					$event_index++;
				}
			}
		}

		$this->orderedEvents = $out;
	}

	/**
	 * Get the list of events as an sorted array
	 *
	 * @param bool $flattened  Whether to remove the nested array structure grouping events by date and time. Defaults to true
	 *
	 * @return array if $flattened=true, an array of Jp_Multi_Event objects. Otherwise an array
	 *               in the format
	 *              [
	 *                  '2022-01-22' => array(
	 *                      '1030' => array(
	 *                          $Jp_Multi_Event,
	 *                          $Jp_Multi_Event
	 *                      ),
	 *                      '1300' => array(
	 *                          $Jp_Multi_Event,
	 *                      ),
	 *                  ),
	 *              ]
	 */
	public function getOrderedEvents ( bool $flattened=true ) : array {
		if ( ! $this->events )
			return [];

		$this->sort_events();

		if ( ! $flattened )
			return $this->orderedEvents;

		$out = [];
		foreach ( $this->orderedEvents as $eventsOnDate ) {
			foreach ( $eventsOnDate as $events ) {
				foreach ( $events as $ev ) {
					$out[] = $ev;
				}
			}
		}
		return $out;
	}


	// @TODO implement paging for events lists
	/**
	 * Get html for the events loaded in a list format
	 *
	 * @param array $options  Array of options for the event list
	 * @param string $options['list_type']  Format to display the events. Accepts "long", "short" (default), and "text"
	 * @param string $options['no_events_text']  Text to display if no events
	 *
	 * @return string
	 */
	public function showEventList ( array $options=[] ) : string {

		$options = array_merge([
			'list_type' => 'short',
			'no_events_text' => 'There are no upcoming events',
			'show_paging' => 'true',
		], $options);

		$this->load_events();
		$this->sort_events();

		if ( ! $this->count_events() )
			return apply_filters( 'jpme_event_list_no_events_text', '<p>' . $options['no_events_text'] . '</p>' );

		$out = '<div class="events-list mejp-upcoming-events events-list-type-' . $options['list_type'] . '">
		<div class="_events">';

		foreach ( $this->getOrderedEvents( false ) as $date => $eventsGroupedByTime ) {
			foreach ( $eventsGroupedByTime as $time => $events ) {
				foreach ( $events as $event ) {
					switch ( $options['list_type'] ) {
						case 'short':
							$out .= $this->getEventSummaryHtml_Short( $event, $date );
							break;
						case 'text':
							$out .= $this->getEventSummaryHtml_Text( $event, $date );
							break;
						case 'long':
						default:
							$out .= $this->getEventSummaryHtml_Long( $event, $date );
							break;
					}
				}
			}
		}
		$out .= '</div><!--._events-->';

		if ( $options['show_paging'] && ( $this->has_prev_page || $this->has_next_page ) ) {
			$out .= '<div class="mejp-events-list-paging">';
			if ( $this->has_prev_page ) {
				$prev_url = add_query_arg( 'evpg', ( $this->current_page - 1 ) );
				$out .= '<a class="jpme-button-link" href="' . esc_attr( $prev_url ) . '">&laquo; Earlier Events</a>';
			}
			if ( $this->has_next_page ) {
				$next_url = add_query_arg( 'evpg', ( $this->current_page + 1 ) );
				$out .= '<a class="jpme-button-link" href="' . esc_attr( $next_url ) . '">Later Events &raquo;</a>';
			}
			$out .= '</div>';
		}

		$out .= '</div>';

		return $out;
	}

	private function getEventSummaryHtml_Short ( Jp_Multi_Event $event, string $date ) : string {
		$hide = $this->hiddenEventParts;

		$hide_classes = array_map( fn( $h ) => "_hide-{$h}", $hide );
		$hide_classes = implode( ' ', $hide_classes );

		$out = '<div class="mejp-event ' . esc_attr( $hide_classes ) . '">';

		if ( ! in_array( 'thumbnail', $hide ) ) {
			$out .= '<div class="mejp-event-thumbnail">';
			$thumb = $event->getFeaturedImage();
			if ( $thumb ) {
				if ( ! in_array( 'link', $hide ) )
					$thumb = "<a href='{$event->getLink()}'>{$thumb}</a>";
				$out .= $thumb;
			}
			$out .= '</div>';
		}

		$out .= '<div>';

		if ( ! in_array( 'date', $hide ) ) {
			$format = ( $event->isEventThisYear( $date ) ) ? 'D, F j' : 'D, F j, Y';
			$out .= "<p class='mejp-ev-datetime'>" . date( $format, strtotime( $date ) ) . ' ';
			if ( ! in_array( 'time', $hide ) && $event->getEventTime() )
				$out .= "&mdash; {$event->getEventTime()}";
			$out .= '</p>';
		}

		if ( ! in_array( 'title', $hide ) ) {
			$title = $event->getTitle();
			if ( ! in_array( 'link', $hide ) )
				$title = '<a href="' . $event->getLink() . '">' . $title . '</a>';
			$out .= "<h3 class='mejp-ev-title'>{$title}</h3>";
		}

		$out .= '</div>
			</div>';

		return $out;
	}

	private function getEventSummaryHtml_Text ( Jp_Multi_Event $event, string $date ) : string {
		$hide = $this->hiddenEventParts;

		$format = ( $event->isEventThisYear() ) ? 'M j' : 'M j Y';
		$formattedDate = date( $format, strtotime( $date ) );

		$calIcon = '<span class="mejp-ev-calIcon">' . file_get_contents( JP_MULTI_EVENTS_PLUGIN_DIR . 'static/calendar2.svg' ) . '</span>';

		$out = <<<HTML
			<div class="mejp-event">
				<a class="mejp-ev-title" href="{$event->getLink()}">
					{$event->getTitle()}
				</a>
				<p class="mejp-ev-datetime">{$calIcon} {$formattedDate} &mdash; {$event->getEventTime()}</p>
			</div>
		HTML;

		return $out;
	}

	private function getEventSummaryHtml_Long ( Jp_Multi_Event $event, string $date ) : string {
		$hide = $this->hiddenEventParts;

		$tags = $event->getEventTags( 'json' );

		$out = '<div data-tags=\'' . $tags . '\' class="events-list-single-event">';

		if ( ! in_array( 'title', $hide ) ) {
			$title = $event->getTitle();
			if ( ! in_array( 'link', $hide ) )
				$title = '<a href="' . $event->getLink() . '">' . $title . '</a>';
			$out .= '<h3 class="mejp-ev-title">' . $title . '</h3>';
		}

		if ( ! in_array( 'thumbnail', $hide ) )
			$out .= '<div class="mejp-ev-image">' . $event->getFeaturedImage( 'medium' ) . '</div>';

		if ( ! in_array( 'date', $hide ) ) {

			$out .= '<p class="mejp-ev-datetime">';

			$out .= date( 'F jS, Y', strtotime( $date ) );

			$time = $event->getEventTime();
			if ($time && ! in_array( 'time', $hide ) )
				$out .= ' &mdash; ' . $time;

			$out .= '</p>';
		}

		if ( ! in_array( 'description', $hide ) )
			$out .= get_the_excerpt( $event->ID );

		if ( ! in_array( 'location', $hide ) )
			$out .= '<div class="mejp-ev-location">' . $event->get_location(false) . '</div>';

		if ( ! in_array( 'tags', $hide ) ) {
			$tags = $event->getEventTags( 'list', 'name' );
			if ( $tags )
				$out .= '<p class="mejp-ev-tags">Event Tags: ' . $tags . '</p>';
		}

		$out .= '</div>';

		return $out;
	}


	public function showCalendar ( string $date='', bool $showFilters=true ) {

		require_once JP_MULTI_EVENTS_PLUGIN_DIR . '/lib/Jp_Multi_CalendarGrid.php';

		$this->setLimit( -1 );
		$this->limitToFutureOrPast = '';

		$d = new DateTime( $date );

		// set start date to the first day of this month
		$d->setDate( $d->format( 'Y' ), $d->format( 'm' ), '1' );
		$this->setStartDate( $d->getTimestamp() );

		// so we can reuse $d in the CalendarGrid constructor
		$d2 = clone $d;
		$d2->add( new DateInterval( 'P1M' ) );
		$this->setEndDate( $d2->getTimestamp() );

		$this->load_events();
		$this->sort_events();

		$cal = new Jp_Multi_CalendarGrid( $d );
		$cal->add_events( $this->getOrderedEvents( false ) );

		return $cal->render();
	}
}
