<?php
defined( 'ABSPATH' ) || exit;

/**
 * These are here only as deprecations for older themes which relied on
 *  using these functions directly.
 */

function mejp_getEvents ( $limit=25, $futureOrPast='future', $showCategory=null, $respectGroupSetting=true ) {
	_doing_it_wrong(
		'mejp_getEvents',
		'This function is deprecated, please use Jp_Multi_Events::instance()->get_events() instead',
		'4.0.0'
	);
	$eventCollection = new Jp_Multi_EventCollection();

	$eventCollection->setLimit( $limit );

	if ( $futureOrPast === 'future' )
		$eventCollection->setToFutureOnly();
	if ( $futureOrPast === 'past' )
		$eventCollection->setToPastOnly();
	if ( $showCategory )
		$eventCollection->setOnlyShowTags( $showCategory );
	if ( ! $respectGroupSetting )
		$eventCollection->setGroupOccassions( false );

	$eventCollection->load_events();
	$eventCollection->sort_events();
	$orderedEvents = $eventCollection->getOrderedEvents( false );
	foreach ( $orderedEvents as $eventsByTime ) {
		foreach ( $eventsByTime as $events ) {
			foreach ( $events as $Event ) {
				$Event->time = $Event->getEventTime();
				$Event->allDates = $Event->getEventDates();
				$Event->date = $Event->getEventDate();
				$Event->futureDates = $Event->getEventDates();
				array_shift( $Event->futureDates );
				$Event->eGroup = $Event->shouldGroupOccurences();
			}
		}
	}
	return $orderedEvents;
}

function mejp_getUpcomingEventCount () : int {
	$eventCollection = new Jp_Multi_EventCollection([
		'limit' => -1,
		'futureOnly' => true,
	]);
	$eventCollection->load_events();
	return $eventCollection->count_events();
}

function jpme_calendarGrid ( $eventsOnDate, $options=array() ) {
	_doing_it_wrong(
		'jpme_calendarGrid',
		'This function is deprecated, please use Jp_Multi_Events::instance()->print_calendar() instead',
		'4.0.0'
	);
	$plugin = Jp_Multi_Events::instance();
	return $plugin->print_calendar();
}

function jpme_calendarGrid_tagFilters ( $onlyShowTheseTags=array() ) {
	_doing_it_wrong(
		'jpme_calendarGrid_tagFilters',
		'This function is deprecated, please use Jp_Multi_events::instance()->print_tag_filters() instead',
		'4.0.0'
	);
	$plugin = Jp_Multi_Events::instance();
	return $plugin->print_tag_filters( $onlyShowTheseTags );
}

function mejp_getEventDate( $format='M d, Y', $id=null ) {
	if ($id === null)
		$id = get_the_ID();
	$ev = Jp_Multi_Events::getEvent( $id );
	return $ev->getEventDate( $format );
}

function mejp_getEventDates( $format='M d, Y', $id=null, $onlyUpcoming=false ) {
	if ($id === null)
		$id = get_the_ID();
	$ev = Jp_Multi_Events::getEvent( $id );
	return $ev->getEventDates( $format, $onlyUpcoming );
}

function mejp_getEventTime( $timeFormat = 'g:i a', $id = null ) {
	if ($id === null)
		$id = get_the_ID();
	$ev = Jp_Multi_Events::getEvent( $id );
	return $ev->getEventTime( $timeFormat );
}

function mejp_getEventEndTime( $timeFormat = 'g:i a', $id = null ) {
	if ($id === null)
		$id = get_the_ID();
	$ev = Jp_Multi_Events::getEvent( $id );
	return $ev->getEventEndTime( $timeFormat );
}

function mejp_getEventTags( $type='array', $id = null ) {
	if ($id === null)
		$id = get_the_ID();
	$ev = Jp_Multi_Events::getEvent( $id );
	return $ev->getEventTags( $type );
}
