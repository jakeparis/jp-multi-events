<?php

class Jp_Multi_Event {

	private $post;
	private int $ID;

	public $dates = null;
	public string $startTime = '';
	public string $endTime = '';

	/**
	 * @param WP_Post|int $post  anything acceptable to get_post
	 *
	 * @throws Exception if the given $post doesn't map to an event
	 */
	public function __construct( $post ) {
		if ( get_post_type( $post ) != 'event' )
			throw new Exception( 'Attempt to wrap a non-event post as an Event' );

		$this->post = get_post( $post );
		$this->ID = $this->post->ID;
	}

	// Allow us to get the post object data directly
	public function __get( $name ) {
		if ( property_exists( $this->post, $name ) )
			return $this->post->{$name};
		return null;
	}

	/**
	 * Clear dates from meta data that are before a given date
	 *
	 * @param string $clearDatesBefore  A date before which to clear dates.
	 *                                 Accepts anything strtotime() accepts
	 *
	 * @return void
	 */
	public function clearOldDates ( $clearDatesBefore='last month' ) {
		$dates = $this->getEventDates( 'Y-m-d' );
		$clearDatesBefore = date( 'Y-m-d', strtotime( $clearDatesBefore ) );
		foreach ( $dates as $date ) {
			if ( $date < $clearDatesBefore ) {
				delete_post_meta( $this->ID, '_Event Date', $date );
			}
		}
	}

	/**
	 * Get the nearest upcoming event date
	 *
	 * @param string $format Format to print the date in
	 *
	 * @return string The date
	 */
	public function getEventDate ( string $format='M d, Y' ) : string {
		$dates = $this->getEventDates( $format, true );
		if ( empty( $dates ) )
			return '';
		return array_shift( $dates );
	}

	/**
	 * Given a date return if it is this year. Defaults to the main date for this event
	 *
	 * @param mixed $date
	 *
	 * @return bool
	 */
	public function isEventThisYear ( $date = null ) : bool {
		if ( ! $date )
			return ( date( 'Y' ) == $this->getEventDate( 'Y' ) );
		if ( is_string( $date ) )
			return ( date( 'Y' ) == date( 'Y', strtotime( $date ) ) );
		if ( $date instanceof DateTime )
			return ( date( 'Y' ) == $date->format( 'Y' ) );
		return false;
	}

	function isAllDay () : bool {
		return ( $this->getEventTime() == '' && $this->getEventEndTime() == '' );
	}

	/**
	 * Get all the dates for this event
	 *
	 * @param string $format How to format the dates
	 * @param bool  $onlyUpcoming  Whether to only show upcoming dates
	 *
	 * @return array An array of event dates as strings in the given format
	 */
	public function getEventDates ( string $format='M d, Y', bool $onlyUpcoming=false ) : array {

		if ( $this->dates === null ) {
			$this->dates = get_post_meta( $this->ID, '_Event Date' );
			if ( ! empty( $this->dates ) )
				sort( $this->dates );
		}

		if ( empty( $this->dates ) )
			return array();

		$dates = $this->dates;

		// remove dates from array if we only want upcoming
		if ( $onlyUpcoming ) {
			$today = date( 'Y-m-d' );
			$dates = array_filter($dates, function ( $date ) use ( $today ) {
				return ( $date >= $today );
			});
		}

		// only reformat the dates if we want a different format than default
		if ( $format != 'Y-m-d' ) {
			array_walk( $dates, function ( &$date ) use ( $format ) {
				$date = date( $format, strtotime( $date ) );
			});
		}

		return $dates;
	}

	/**
	 * Set the dates for this event.
	 *
	 * @param string|array $dates  a string separated by \n or an array. Each date
	 *                             can be in any format accepted by strtotime
	 *                             and is converted into Y-m-d for storage
	 *
	 * @return void
	 */
	public function setEventDates ( $dates ) {
		$post_id = $this->ID;

		if ( ! is_array( $dates ) )
			$dates = explode( "\n", $dates );

		// remove empty values
		$dates = array_filter( $dates );

		delete_post_meta( $post_id, '_Event Date' );

		array_walk( $dates, function ( $date ) use ( $post_id ) {
			$date = date( 'Y-m-d', strtotime( $date ) );
			add_post_meta( $post_id, '_Event Date', $date );
		});
	}

	/**
	 * Get the event time
	 *
	 * @param string $format
	 *
	 * @return string
	 */
	public function getEventTime ( string $format='g:i a' ) : string {
		if ( empty( $this->startTime ) )
			$this->startTime = get_post_meta( $this->ID, '_Event Time', true );
		if ( empty( $this->startTime ) )
			return '';
		return date( $format, strtotime( $this->startTime ) );
	}

	/**
	 * Set the event time
	 *
	 * @param string $v The event time in any acceptable format. If empty,
	 *                  the meta field will be removed
	 *
	 * @return void
	 */
	public function setEventTime ( string $v ) {
		if ( empty( $v ) ) {
			delete_post_meta( $this->ID, '_Event Time' );
			return;
		}
		$v = date( 'H:i', strtotime( $v ) ); // 24 hour time
		update_post_meta( $this->ID, '_Event Time', $v );
	}

	/**
	 * Get the event end time
	 *
	 * @param string $format
	 *
	 * @return string
	 */
	public function getEventEndTime ( string $format='g:i a' ) : string {
		if ( empty( $this->endTime ) )
			$this->endTime = get_post_meta( $this->ID, '_EventEndTime', true );
		if ( empty( $this->endTime ) )
			return '';
		return date( $format, strtotime( $this->endTime ) );
	}

	/**
	 * Set the event end time
	 *
	 * @param string $v The event time in any acceptable format. If empty,
	 *                  the meta field will be removed
	 *
	 * @return void
	 */
	public function setEventEndTime ( string $v ) {
		if ( empty( $v ) ) {
			delete_post_meta( $this->ID, '_EventEndTime' );
			return;
		}
		$v = date( 'H:i', strtotime( $v ) ); // 24 hour time
		update_post_meta( $this->ID, '_EventEndTime', $v );
	}


	/**
	 * Get the event tags (either slug or name)
	 *
	 * @param string $type  How to return the tags: "array" (default), "json" (for use in data attributes), "list" a string list, separated by $listSeparator
	 * @param string $returnfield Which field to get from Term: "slug" (default) or "name"
	 * @param string $listSeparator  If $type is "list", how to separate the terms
	 *
	 * @return void
	 */
	public function getEventTags ( string $type='array', string $returnfield='slug', $listSeparator=', ' ) {
		$tags = get_the_terms( $this->ID, 'event-tag' );
		if ( ! $tags ) {
			switch ( $type ) {
				case 'array':
					return [];
				case 'json':
					return '[]';
				case 'list':
					return '';
				default:
					return false;
			}
		}

		// only allow terms that will exist as a prop on WP_Term
		$returnfield = ( in_array( $returnfield, [ 'name','slug' ] ) )
			? $returnfield
			: 'slug';

		$tagList = array_map(function ( $Term ) use ( $returnfield, $type ) {
			if ( $type == 'list' && $returnfield == 'name' ) {
				$link = get_term_link( $Term, 'event-tag' );
				return '<a href="' . $link . '">' . $Term->name . '</a>';
			}
			return $Term->{$returnfield};
		}, $tags);

		switch ( $type ) {
			// case 'json' :
			//  $out = json_encode($tags);
			//  break;
			case 'json':
				$out = json_encode( $tagList );
				break;
			case 'list':
				$out = implode( $listSeparator, $tagList );
				break;
			case 'array':
			default:
				$out = $tagList;
				break;
		}
		return $out;
	}

	public function shouldGroupOccurences () {
		return get_post_meta( $this->ID, '_Event Group Occasions', true ) == '1';
	}

	public function setGroupOccurences ( bool $b ) {
		$b = ( $b ) ? '1' : '0';
		update_post_meta( $this->ID, '_Event Group Occasions', $b );
	}

	public function getLink () {
		return get_permalink( $this->ID );
	}

	public function getTitle () {
		return apply_filters( 'the_title', $this->post->post_title, $this->ID );
	}

	public function get_location ( $directions_link=true, $add_line_breaks=false ) {
		$loc = get_post_meta( $this->ID, '_event_location', true );
		if ( empty( $loc ) )
			return $loc;
		if ( $add_line_breaks ) {
			$bits = explode( ',', $loc );
			array_walk( $bits, 'trim' );
			$loc = implode( "<br>\n", $bits );
		}
		if ( $directions_link ) {
			$directions_url = $this->get_directions_url();
			if ( $directions_url ) {
				$loc = '<a href="' . esc_attr( $directions_url ) . '" target="_blank" title="open in larger map">' . $loc . '</a>';
			}
		}
		return $loc;
	}

	public function get_location_coords () {
		$coords = get_post_meta( $this->ID, '_event_location_coords', true );
		if ( empty( $coords ) )
			return $coords;
		return json_decode( $coords );
	}

	public function get_map_image () {
		$coords = $this->get_location_coords();
		if ( empty( $coords ) )
			return false;
		require_once 'Jp_Multi_Events_Map_Api.php';
		return Jp_Multi_Events_Map_Api::get_map_image_url( $coords->lat, $coords->lng );
	}

	public function get_dynamic_map () {
		$coords = $this->get_location_coords();
		if ( ! $coords ) {
			return false;
		}
		require_once 'Jp_Multi_Events_Map_Api.php';
		return Jp_Multi_Events_Map_Api::get_dynamic_map( $coords->lat, $coords->lng );
	}

	public function get_directions_url () {
		$coords = $this->get_location_coords();
		if ( ! $coords ) {
			return false;
		}
		require_once 'Jp_Multi_Events_Map_Api.php';
		return Jp_Multi_Events_Map_Api::get_directions_url( $coords->lat, $coords->lng );
	}

	/**
	 * Get the featured image for the event
	 *
	 * @param mixed $size can be a registered image size or an array. See get_the_post_thumbnail() for details
	 *
	 * @return string html for the image tag
	 */
	public function getFeaturedImage ( $size=array( 100, 100 ) ) : string {
		return get_the_post_thumbnail( $this->ID, $size );
	}

}
