<?php
require_once JP_MULTI_EVENTS_PLUGIN_DIR . 'lib/jp-core/Jp_Plugin_Core_Admin.php';
require_once JP_MULTI_EVENTS_PLUGIN_DIR . 'lib/jp-core/Jp_Cpt_Core.php';
require_once JP_MULTI_EVENTS_PLUGIN_DIR . 'inc/Jp_Multi_Events_Common.php';

class Jp_Multi_Events_Admin extends Jp_Plugin_Core_Admin {
	use Jp_Multi_Events_Common;

	private function __construct() {

		$this->register_post_type();
		$this->register_hooks();
	}

	private function register_hooks () {
		add_action( 'admin_init', [ $this, 'on_admin_init' ] );
		add_action( 'block_categories_all', [ $this, 'on_block_categories' ], 10, 2 );
		add_action( 'event-tag_add_form_fields', [ $this, 'add_tag_form_fields' ] );
		add_action( 'event-tag_edit_form_fields', [ $this, 'add_tag_form_fields' ] );
		add_action( 'saved_term', [ $this, 'on_save_tag' ], 10, 3 );
		add_action( 'rest_api_init', [ $this, 'setup_rest_endpoints' ] );
	}

	public function on_block_categories ( $categories, $block_editor_context ) {
		if ( ! ( $block_editor_context instanceof WP_Block_Editor_Context ) )
			return $categories;

		if ( $block_editor_context->post->post_type != 'event' )
			return $categories;

		return array_merge( $categories, [
			[
				'slug' => 'jp-events',
				'title' => 'Events',
				// 'icon' => 'calendar-alt',
			],
		]);
	}

	public function setup_rest_endpoints () {

		register_rest_route( 'jp-events/v1',
			'/lookupaddress/',
			[
				'methods' => 'GET',
				'permission_callback' => fn () => current_user_can( 'edit_posts' ),
				'callback' => [ $this, 'rest_lookup_address' ],
			]
		);
		register_rest_route( 'jp-events/v1',
			'/getmap/',
			[
				'methods' => 'POST',
				'permission_callback' => fn () => current_user_can( 'edit_posts' ),
				'callback' => [ $this, 'rest_getmap_image_url' ],
			]
		);
	}

	public function rest_lookup_address ( WP_REST_Request $request ) {
		require_once 'Jp_Multi_Events_Map_Api.php';
		$params = $request->get_query_params();
		return Jp_Multi_Events_Map_Api::get_locations_by_address( $params['lookupaddress'] );
	}

	public function rest_getmap_image_url ( WP_REST_Request $request ) {
		require_once 'Jp_Multi_Events_Map_Api.php';
		$incoming = $request->get_body_params();
		$coords = json_decode( $incoming['coords'] );
		return Jp_Multi_Events_Map_Api::get_map_image_url( $coords->lat, $coords->lng );
	}

	public function on_admin_init () {

		$this->enqueueAdminFiles();

		$this->eventsPostCore->add_admin_column('event-date', 'Upcoming Event Dates', function ( $post_id ) {
			$Event = Jp_Multi_Events::getEvent( $post_id );
			$dates = $Event->getEventDates( 'M d, Y', true );
			if ( ! $dates ) {
				echo '<p style="color:gray"><i>No upcoming dates</i></p>';
				return;
			}
			echo '<ul>';
			$i = 0;
			foreach ( $dates as $d ) {
				if ( $i > 3 ) {
					echo '<li><i>more &hellip;</i></li>';
					break;
				}
				echo "<li>{$d}</li>";
				$i++;
			}
			echo '</ul>';
		});

		$this->eventsPostCore->add_admin_column( 'event-location', 'Event Location', function ( $post_id ) {
			$event = Jp_Multi_Events::getEvent( $post_id );
			$loc = $event->get_location( false );
			if ( $loc )
				echo $loc;
		});

		$this->eventsPostCore->remove_admin_column( 'date' );

		// 5th param tells it don't run in block editor
		$dateMetaBox = $this->eventsPostCore->add_meta_box(
			'mejp_metaEventBox',
			'Event Date & Time',
			'side',
			'low',
			false
		);

		$dateMetaBox->onRender(function ( $post ) {
			$wp_screen = get_current_screen();
			if ( $wp_screen->is_block_editor() ) {
				echo '<p>Event settings have moved! If you don\'t see
				them at the top of your editor (just below the event title), click the <b>three-dot menu</b> (far upper right of the screen), and click <b>"Event Controls"</b>.</p>';
				return;
			}

			$Event = self::getEvent( $post );
			$dates = $Event->getEventDates();
			$datesStr = trim( implode( "\n", $dates ) );
			$startTime = $Event->getEventTime();
			$endTime = $Event->getEventEndTime();
			$groupOccurencesChecked = $Event->shouldGroupOccurences() ? ' checked ' : '';

			$html = <<<HTML
			<div class="jp-multi-events">
			
				<p class="incidental">Put one date on each line.  Any date format should work.</p>
			
				<label for="event_dates"><b>Event Dates</b></label>
			
				<textarea id="pickdateJP" name="event_dates" rows="5" style="margin:0 5px 0 0">{$datesStr}</textarea>


				<label for="event_time">
					<b>Start Time</b>
				</label>
				
				<input name="event_time" type="text" value="{$startTime}" />
				
				<br>

				<label for="event_end_time">
					<b>End Time</b>
				</label>
			
				<input name="event_end_time" type="text" value="{$endTime}" />
				
				<p class="group-instances-wrap">
					<input name="event_group_occurences" value="1" id="event_group_occurences" type="checkbox" {$groupOccurencesChecked}> 
					<label style="font-style:italic;font-size:.9em;" for="event_group_occurences">Group multiple instances together in lists. When checked, only the nearest event will show in lists. If unchecked, the event listing will repeat for each date held.</label>
				</p>
				<input name="saveEventInfo" type="hidden" value="1" />
			
			</div>
HTML;
			echo $html;
		});

		$dateMetaBox->onSave(function ( $post_id ) {

			$Event = self::getEvent( $post_id );

			if ( isset( $_POST['event_group_occurences'] ) )
				$Event->setGroupOccurences( true );
			else {
				$Event->setGroupOccurences( false );
			}

			if ( isset( $_POST['event_time'] ) )
				$Event->setEventTime( $_POST['event_time'] );
			if ( isset( $_POST['event_end_time'] ) )
				$Event->setEventEndTime( $_POST['event_end_time'] );
			if ( isset( $_PoST['event_dates'] ) )
				$Event->setEventDates( $_POST['event_dates'] );
		});
	}


	/**
	 * Enqueue the admin files not associated with blocks (ui)
	 *
	 * @return void
	 */
	public function enqueueAdminFiles () {
		$cssHandles = $this->get_blocks_manager()->getRegisteredAssetNames(
			'jp-multi-events/ui',
			'style'
		);
		$adminCssHandles = $this->get_blocks_manager()->getRegisteredAssetNames(
			'jp-multi-events/ui',
			'editorStyle'
		);
		$jsHandles = $this->get_blocks_manager()->getRegisteredAssetNames(
			'jp-multi-events/ui',
			'editorScript'
		);

		if ( $cssHandles ) {
			foreach ( $cssHandles as $cssHandle ) {
				wp_enqueue_style( $cssHandle );
			}
		}
		if ( $adminCssHandles ) {
			foreach ( $adminCssHandles as $adminCssHandle ) {
				wp_enqueue_style( $adminCssHandle );
			}
		}
		if ( $jsHandles ) {
			foreach ( $jsHandles as $jsHandle ) {
				wp_enqueue_script( $jsHandle );
			}
		}
	}

	public function add_tag_form_fields ( $thing ) {

		wp_enqueue_script( 'wp-color-picker' );
		wp_enqueue_style( 'wp-color-picker' );

		$inputval = '';
		// If this is the "add" form, $thing is a string for the taxonomy name
		if ( $thing instanceof WP_Term )
			$inputval = get_term_meta( $thing->term_id, 'tag-color', true );

		?>
	
			<tr class="form-field tag-color-wrap">
				<th scope="row">
					<label for="tag-color">Tag Color</label>
				</th>
				<td>
					<input class="color-field" id="tag-color" name="tag-color" type="text" value="<?= esc_attr( $inputval ) ?>">
					<p class="description">This color will be used to highlight events on the calendar. </p>
				</td>
			</tr>
	
			<script>
				(function() {
					jQuery(function() {
						jQuery('.color-field').wpColorPicker();
					});
				})();
			</script>
		<?php
	}

	public function on_save_tag ( $term_id, $tt_id, $tax ) {
		if ( $tax !== 'event-tag' )
			return;

		if ( isset( $_POST['tag-color'] ) )
			update_term_meta( $term_id, 'tag-color', $_POST['tag-color'] );
	}
}