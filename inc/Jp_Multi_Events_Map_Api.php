<?php

class Jp_Multi_Events_Map_Api {

	private static function get_map_api_key () {
		// @TODO make configurable/switch-out-able
		return 'hlqCcYAKx-3szodhZL7RDfFDmKrJBTevYrAo3xEPsrQ';
	}


	public static function get_map_image_url ( $lat, $lng ) {
		if ( empty( $lat ) || empty( $lng ) ) {
			return false;
		}
		$params = [
			'overlay' => "point:{$lat},{$lng}",
			'apikey' => self::get_map_api_key(),
		];
		$url = 'https://image.maps.hereapi.com/mia/v3/base/mc/overlay:zoom=16/400x220/jpeg?' . http_build_query( $params );

		return $url;
	}


	public static function get_directions_url ( $lat, $lng ) {
		// $url = "https://www.google.com/maps/dir//{$lat},{$lng}/@{$lat},{$lng},16z";
		$url = "https://www.google.com/maps/search/{$lat},{$lng}?entry=tts";
		return $url;
	}


	public static function get_locations_by_address ( $address ) {
		if ( empty( $address ) )
			return [];
		$params = http_build_query([
			'q' => $address,
			'apikey' => self::get_map_api_key(),
		]);
		$url = 'https://geocode.search.hereapi.com/v1/geocode?' . $params;

		$resp = wp_remote_get( $url );
		if ( is_wp_error( $resp ) ) {
			error_log( 'ERROR: ' . $resp->get_error_message() );
			return false;
		}
		// $resp_code = wp_remote_retrieve_response_code( $resp );
		$resp_body = wp_remote_retrieve_body( $resp );
		$out = json_decode( $resp_body );

		if ( empty( $out->items ) )
			return [];

		return $out->items;
	}


	public static function get_dynamic_map ( $lat, $lng, $options=[] ) {
		$here_version = 'v3/3.1';

		$options = array_merge([
			'controls' => false,
		], $options );

		$lat = (float) $lat;
		$lng = (float) $lng;
		$apikey = self::get_map_api_key();

		wp_enqueue_script( 'here-map-core',
			"https://js.api.here.com/{$here_version}/mapsjs-core.js",
			[]
		);
		wp_enqueue_script( 'here-map-service',
			"https://js.api.here.com/{$here_version}/mapsjs-service.js",
			[
				'here-map-core',
			]
		);
		wp_enqueue_script( 'here-map-dynamic',
			"https://js.api.here.com/{$here_version}/mapsjs-mapevents.js",
			[
				'here-map-core',
				'here-map-service',
			]
		);

		$map_controls_js = '';
		if ( $options['controls'] ) {

			wp_enqueue_script( 'here-map-controls',
				"https://js.api.here.com/{$here_version}/mapsjs-ui.js",
				[
					'here-map-core',
					'here-map-service',
					'here-map-dynamic',
				]
			);
			wp_enqueue_style( 'here-map-controls',
				"https://js.api.here.com/{$here_version}/mapsjs-ui.css",
				[]
			);
			$map_controls_js = '
				const ui = H.ui.UI.createDefault(map, defaultLayers);
			';
		}

		$html = <<<HTML
			<div style="width: 600px; height: 300px" id="location-map-container"></div>
			<script>
			document.addEventListener( 'DOMContentLoaded', function(){
				const mapPlatform = new H.service.Platform({'apikey':'{$apikey}'});
				const defaultLayers = mapPlatform.createDefaultLayers();
				const center = {lat: {$lat}, lng: {$lng}};
				const map = new H.Map(
					document.getElementById('location-map-container'),
					defaultLayers.vector.normal.map,
					{
						zoom: 16,
						center: center
					}
				);
				
				const iconSvg = '<svg width="58" height="58" viewBox="0 0 42 42" xmlns="http://www.w3.org/2000/svg" ><defs><style>.cls-1{fill:url(#linear-gradient);}</style><linearGradient gradientUnits="userSpaceOnUse" id="linear-gradient" x1="12.22" x2="37.03" y1="24" y2="24"><stop offset="0" stop-color="#ca336d"/><stop offset="1" stop-color="#9e2d4f"/></linearGradient></defs><path class="cls-1" d="M18.72,8.31a10.79,10.79,0,0,0-5.17,5.24A12.74,12.74,0,0,0,14.7,26.81l9.93,14.25,9.92-14.25A12.33,12.33,0,0,0,37,19.36C37,10.6,28,4,18.72,8.31Zm6.08,15.2A4.51,4.51,0,1,1,29.3,19,4.51,4.51,0,0,1,24.8,23.51Z"/></svg>';
				const icon = new H.map.Icon(iconSvg);
				const marker = new H.map.Marker( center , { icon: icon } );
				map.addObject(marker);
				// map.setCenter(center);
				const behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents (map ));
				window.addEventListener( 'resize', () => map.getViewPort().resize() );
				{$map_controls_js}
			});
			</script>
		HTML;
		return $html;
	}

}