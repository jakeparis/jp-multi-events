<?php
require_once JP_MULTI_EVENTS_PLUGIN_DIR . 'lib/jp-core/Jp_Cpt_Core.php';

trait Jp_Multi_Events_Common {

	private static $instance = null;
	protected $eventsPostCore = null;
	public string $pluginSlug = 'jp-multi-events';
	public string $version = JP_MULTI_EVENTS_PLUGIN_VERSION;


	/**
	 * Get a singleton instance of this class
	 *
	 * @return Jp_Multi_Events|Jp_Multi_Events_Admin
	 */
	public static function instance () {
		if ( self::$instance === null ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Factory function to get a post wrapped as an Event
	 *
	 * @param mixed $post A post id, or a post object
	 */
	public static function getEvent ( $post ): Jp_Multi_Event {
		return new Jp_Multi_Event( $post );
	}

	/**
	 * Enqueue the public files not associated with blocks (ui)
	 *
	 * @return void
	 */
	public function enqueuePublicFiles () {
		$cssHandles = $this->get_blocks_manager()->getRegisteredAssetNames(
			'jp-multi-events/ui',
			'style'
		);
		$jsHandles = $this->get_blocks_manager()->getRegisteredAssetNames(
			'jp-multi-events/ui',
			'viewScript'
		);
		if ( $cssHandles ) {
			foreach ( $cssHandles as $cssHandle ) {
				wp_enqueue_style( $cssHandle );
			}
		}
		if ( $jsHandles ) {
			foreach ( $jsHandles as $jsHandle ) {
				wp_enqueue_script( $jsHandle );
			}
		}
	}


	private function register_post_type () {
		if ( post_type_exists( 'event' ) ) {
			$this->eventsPostCore = new Jp_Cpt_Core( 'event' );
			return;
		}
		// $icon = base64_encode( file_get_contents( JP_MULTI_EVENTS_PLUGIN_DIR . '/static/calendar3.svg' ) );
		// hard code it so we don't need to do the work each time
		$icon = 'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iNTEycHQiIGhlaWdodD0iNTEycHQiIHZlcnNpb249IjEuMSIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CiA8ZyBmaWxsPSIjZmZmIj4KICA8cGF0aCBkPSJtNDYzLjA1IDE0MC4wNmgxNS42OTl2LTE3Ljk3N2MwLTI0LjUyMy0xOS45NTMtNDQuNDczLTQ0LjQ3Ny00NC40NzNoLTIuNjcxOXYtMjIuMTE3Yy0wLjAwMzkwNi0xNi40ODgtMTMuNDA2LTI5Ljg5MS0yOS44ODctMjkuODkxaC0yLjIyNjZjLTE2LjQ3NyAwLTI5Ljg4NyAxMy40MDItMjkuODg3IDI5Ljg4N3YyMi4xMTdoLTgyLjU5OHYtMjIuMTE3YzAtMTYuNDg0LTEzLjQwNi0yOS44ODctMjkuODkxLTI5Ljg4N2gtMi4yMjI3Yy0xNi40OCAwLTI5Ljg5MSAxMy40MDItMjkuODkxIDI5Ljg4N3YyMi4xMTdoLTgyLjU5NHYtMjIuMTE3YzAtMTYuNDg0LTEzLjQwMi0yOS44ODctMjkuODg3LTI5Ljg4N2gtMi4yMjI3Yy0xNi40OCAwLTI5Ljg5MSAxMy40MDItMjkuODkxIDI5Ljg4N3YyMi4xMTdoLTIuNjcxOWMtMjQuNTIzIDAtNDQuNDc3IDE5Ljk1My00NC40NzcgNDQuNDczdjE3Ljk3N2gxNS42OTlsNDE0LjA5LTAuMDAzOTA3eiIvPgogIDxwYXRoIGQ9Im00OC45NTcgMTYxLjAyaC0xNS43MDN2MjgwLjljMCAyNC41MiAxOS45NTMgNDQuNDczIDQ0LjQ3NyA0NC40NzNoMzU2LjU0YzI0LjUyMyAwIDQ0LjQ3Ny0xOS45NTMgNDQuNDc3LTQ0LjQ3M2wwLjAwMzkwNi0yODAuOXptNTIuNDE0IDk5Ljg2N3YtNTUuMzI4aDg1LjYxN3Y1NS4zMjh6bTAgODEuNTM5di01NS4zMzJoODUuNjE3djU1LjMyOGgtODUuNjE3em0wIDgxLjUzNXYtNTUuMzI4aDg1LjYxN3Y1NS4zMjhsLTg1LjYxNy0wLjAwMzkwNnptMTExLjgyLTE2My4wN3YtNTUuMzI4aDg1LjYxM3Y1NS4zMjh6bTAgODEuNTM5di01NS4zMzJoODUuNjEzdjU1LjMyOGgtODUuNjEzem0wIDgxLjUzNXYtNTUuMzI4aDg1LjYxM3Y1NS4zMjhsLTg1LjYxMy0wLjAwMzkwNnptMTExLjgyLTE2My4wN3YtNTUuMzI4aDg1LjYxM3Y1NS4zMjh6bTAgODEuNTM5di01NS4zMzJoODUuNjEzdjU1LjMyOGgtODUuNjEzeiIvPgogPC9nPgo8L3N2Zz4K';
		$this->eventsPostCore = new Jp_Cpt_Core( 'event',
			array(
				'labels' => array(
					'name' => 'Events',
					'singular_name' => 'Event',
					'add_new' => 'New Event',
					'add_new_item' => 'Create New Event',
					'edit_item' => 'Edit Event',
					'new_item' => 'New Event',
					'view_item' => 'View Event Listing',
					'search_items' => 'Search Event Listings',
					'not_found' => 'No events found',
					'not_found_in_trash' => 'No events in trash',
					'show_in_nav_menus' => false,
				),
				'public' => true,
				'description' => 'Event Listings',
				'supports' => array(
					'title',
					'editor',
					'thumbnail',
					'custom-fields',
				),
				'menu_position' => 20,
				 'menu_icon' => JP_MULTI_EVENTS_PLUGIN_URL . '/static/calendar2.svg',
//				'menu_icon' => "data:image/svg+xml;base64,{$icon}",
				'rewrite' => array(
					'slug' => 'events',
					'with_front' => false,
				),
				'taxonomies' => array( 'event-tag' ),
				'show_in_rest' => true,
				'template' => array(
					[
						'jp-multi-events/event-meta',
						[
							'lock' => [
								'move' => true,
								'remove' => true,
							],
						],
					],
					[
						'core/paragraph',
						[
							'placeholder' => 'Enter your event description here...',
						],
					],
				),
			)
		);
	}

}
