<?php
defined( 'ABSPATH' ) || exit;

add_action('init', function () {

	add_shortcode( 'events-list', function ( $atts ) {
		$atts = shortcode_atts(array(
			'limit' => 25,
			'hide' => '', // comma-separated list of parts to hide. possible values: tags,description,link,title,date,time,thumbnail
			'hide_parts' => '', // alias of "hide"
			'list_type' => 'long',
			'all-events-link' => '#',

			'only-tags' => '', // comma-sep list of tags to include
			'only-past' => '', // set to "past" for backwards looking list

		), $atts, 'events-list');

		if ( ! empty( $atts['only-tags'] ) ) {
			$atts['onlyTags'] = $atts['only-tags'];
			unset( $atts['only-tags'] );
		}
		if ( ! empty( $atts['only-past'] ) ) {
			$atts['futureOrPast'] = 'past';
			unset( $atts['only-past'] );
		}
		$Plugin = Jp_Multi_Events::instance();

		return $Plugin->print_event_list( $atts );
	});

	add_shortcode('events-calendar-grid', function ( $atts ) {

		$atts = shortcode_atts(array(
			'tag' => '',
			'show-filters' => true, // pass "" or 0 to not show the filter bar
			'only-show-these-tags-in-filter-list' => '', // comma-sep list of tags to show int he filter bar
		), $atts, 'events-calendar-grid' );

		$Plugin = Jp_Multi_Events::instance();
		$Plugin->get_blocks_manager()->enqueuePublicJs( 'jp-multi-events/calendar-grid' );

		$atts['date'] = ( isset( $_GET['jpme_date'] ) ) ? $_GET['jpme_date'] : 'today';

		if ( ! empty( $atts['only-show-these-tags-in-filter-list'] ) ) {
			$atts['onlyShowTheseTagsInFilterList'] = $atts['only-show-these-tags-in-filter-list'];
			unset( $atts['only-show-these-tags-in-filter-list'] );
		}
		if ( $atts['show-filters'] !== true ) {
			$atts['showFilters'] = $atts['show-filters'];
			unset( $atts['show-filters'] );
		}

		return $Plugin->print_calendar( $atts );
	});
});
