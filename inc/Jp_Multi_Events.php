<?php
require_once JP_MULTI_EVENTS_PLUGIN_DIR . 'lib/jp-core/Jp_Plugin_Core.php';
require_once JP_MULTI_EVENTS_PLUGIN_DIR . 'lib/jp-core/Jp_Cpt_Core.php';
require_once JP_MULTI_EVENTS_PLUGIN_DIR . 'inc/Jp_Multi_Events_Common.php';

class Jp_Multi_Events extends Jp_Plugin_Core {
	use Jp_Multi_Events_Common;

	private function __construct() {

		$this->register_post_type();
		$this->register_actions();

		$this->get_blocks_manager()->register_blocks( 'build' );
	}

	private function register_actions () {
		add_action( 'init', [ $this, 'on_init' ] );
		add_action( 'wp', [ $this, 'maybe_filter_content' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_things' ] );
	}

	public function on_init () {
		register_taxonomy('event-tag', 'event', array(
			'label' => 'Event Tags',
			'labels' => array(
				'name' => 'Event Tags',
				'singular_name' => 'Event Tag',
			),
			'public' => true,
			'show_in_rest' => true,
		));

		register_rest_field( 'event-tag', 'tag_color', [
			'get_callback' => function ( $obj ) {
				return get_term_meta( $obj['id'], 'tag-color', true );
			},
		]);

		register_post_meta( 'event', '_Event Date', [
			'object_subtype' => 'event',
			'show_in_rest' => true,
			'type' => 'string',
			'single' => false,
			'auth_callback' => fn() => current_user_can( 'edit_posts' ),
			'sanitize_callback' => function ( $value ) {
				if ( ! empty( $value ) )
					$value = date( 'Y-m-d', strtotime( $value ) );
				return $value;
			},
		]);
		register_post_meta( 'event', '_Event Time', [
			'object_subtype' => 'event',
			'show_in_rest' => true,
			'type' => 'string',
			'single' => true,
			'auth_callback' => fn() => current_user_can( 'edit_posts' ),
			'sanitize_callback' => function ( $value ) {
				if ( ! empty( $value ) )
					$value = date( 'H:i', strtotime( $value ) );
				return $value;
			},
		]);
		register_post_meta( 'event', '_EventEndTime', [
			'object_subtype' => 'event',
			'show_in_rest' => true,
			'type' => 'string',
			'single' => true,
			'auth_callback' => fn() => current_user_can( 'edit_posts' ),
			'sanitize_callback' => function ( $value ) {
				if ( ! empty( $value ) )
					$value = date( 'H:i', strtotime( $value ) );
				return $value;
			},
		]);
		register_post_meta( 'event', '_Event Group Occasions', [
			'object_subtype' => 'event',
			'show_in_rest' => true,
			'type' => 'boolean',
			'single' => true,
			'auth_callback' => fn() => current_user_can( 'edit_posts' ),
		]);
		register_post_meta( 'event', '_event_location', [
			'object_subtype' => 'event',
			'show_in_rest' => true,
			'type' => 'string',
			'single' => true,
			'auth_callback' => fn() => current_user_can( 'edit_posts' ),
		]);

		register_post_meta( 'event', '_event_location_coords', [
			'object_subtype' => 'event',
			'show_in_rest' => true,
			'type' => 'string',
			'single' => true,
			'auth_callback' => fn() => current_user_can( 'edit_posts' ),
		]);
	}

	public function enqueue_things () {
		$this->enqueuePublicFiles();
	}

	public function maybe_filter_content () {
		if ( ! is_singular( 'event' ) )
			return;
		add_filter( 'the_content', [ $this, 'filter_content' ] );
	}

	public function filter_content ( $content ) {
		// we've already ensured this is a single event
		$id = get_the_ID();
		$event = self::getEvent( $id );

		$prevDates = array();
		$dates = $event->getEventDates( 'M d, Y', true );
		if ( ! $dates ) {
			$prevDates = $event->getEventDates( 'M d, Y', false );
			if ( ! $prevDates )
				return $content;
		}

		// $this->enqueuePublicFiles();

		$time = $event->getEventTime();
		$endTime = $event->getEventEndTime();

		if ( $time ) {
			$endTime = ( $endTime ) ? ' - ' . $endTime : '';
			$time = '<span class="jpme-event-single-view-meta_time">' . $time . $endTime . '</span>';
		}

		$eventMetaHtml = '';
		if ( count( $dates ) ) {
			// render nearest date differently
			$currentDate = array_shift( $dates );
			$eventMetaHtml .= '<div class="jpme-event-single-view-meta">
				<div class="jpme-event-single-view-dates">
					<span class="jpme-event-single-view-meta_date">' . $currentDate . '</span><br>
					' . $time;

			if ( count( $dates ) > 1 ) {
				$eventMetaHtml .= '<div class="jpme-event-single-view-upcoming-dates-wrap">
					<p>
						<a href="#" class="js-opener jpme-view-upcoming-opener">Other upcoming dates &hellip;</a>
					</p>
					<ul class="jpme-view-upcoming-dates">';
				foreach ( $dates as $d ) {
					$eventMetaHtml .= "<li>{$d}</li>\n";
				}
				$eventMetaHtml .= '</ul></div>';
			}
			$eventMetaHtml .= '</div>';
		} elseif ( ! empty( $prevDates ) ) {
			$eventMetaHtml .= '<div class="jpme-event-single-view-meta">
				<div class="jpme-event-single-view-past-dates-wrap">
				<p>
					This event is past. 
				</p>
				<ul>';
			foreach ( $prevDates as $d ) {
				$eventMetaHtml .= "<li>{$d}</li>\n";
			}
			$eventMetaHtml .= '</ul>
			</div>';
		}

		$eventMetaHtml .= '<div class="jpme-event-single-view-location">';
		$loc = $event->get_location();
		if ( $loc ) {
			$eventMetaHtml .= '<p>' . apply_filters( 'the_title', $loc, $id ) . '</p>';
			$eventMetaHtml .= $event->get_dynamic_map();
		}

		$eventMetaHtml .= '</div>
			</div>';

		$tags = $this->get_event_tags_meta( get_the_ID() );

		return $eventMetaHtml . $content . $tags;
	}

	public function get_event_tags_meta ( $post, $label='', $separator='' ) {
		$post = get_post( $post );

		$terms = get_object_term_cache( $post->ID, 'event-tag' );
		if ( false === $terms ) {
			$terms = wp_get_object_terms( $post->ID, 'event-tag' );
		}
		if ( empty( $terms ) )
			return '';

		$links = [];
		$link_str = '<a href="%s" class="jpme-button-link">%s</a>';
		foreach ( $terms as $term ) {
			$links[] = wp_sprintf( $link_str, esc_attr( get_term_link( $term ) ), $term->name );
		}

		$label = ( $label ) ? $label : 'Find related events: ';
		return '<div class="jpme-event-tags"><span class="_label">' . $label . '</span>
		<span class="_links">' . implode( $separator, $links ) . '</span></div>';
	}


	/**
	 * Generic function to get events. Is a wrapper around Jp_Multi_Collection()
	 *
	 * @param array $args
	 * @param string|int $args[limit] How many events to show
	 * @param bool $args[futureOnly] Whether to only show future events
	 * @param bool $args[pastOnly] Whether to only show past events (overriden by futureOnly)
	 * @param string $args[onlyTags] comma-sep list of tag slugs to show events for (defaults everything)
	 * @param bool $args[respectGroupSetting] whether to group events (defaults true)
	 *
	 * @return array An array of Jp_Multi_Event objects
	 */
	public function get_events ( array $args=[] ) : array {
		$eventCollection = new Jp_Multi_EventCollection();

		if ( isset( $args['limit'] ) )
			$eventCollection->setLimit( $args['limit'] );

		if ( isset( $args['pastOnly'] ) && $args['pastOnly'] )
			$eventCollection->setToPastOnly();

		if ( isset( $args['futureOnly'] ) && $args['futureOnly'] )
			$eventCollection->setToFutureOnly();

		if ( $args['onlyTags'] )
			$eventCollection->setOnlyShowTags( $args['onlyTags'] );

		if ( $args['respectGroupSetting'] )
			$eventCollection->setGroupOccassions( $args['respectGroupSetting'] );

		$eventCollection->load_events();
		$eventCollection->sort_events();
		return $eventCollection->getOrderedEvents();
	}

	/**
	 * Print the html for the event listing
	 *
	 * @param array $args
	 * @param string|int $args[limit] How many events to show
	 * @param bool $args[futureOnly] Whether to only show future events
	 * @param bool $args[pastOnly] Whether to only show past events (overriden by futureOnly)
	 * @param string $args[onlyTags] comma-sep list of tag slugs to show events for (defaults everything)
	 * @param bool $args[respectGroupSetting] whether to group events (defaults true)
	 * @param string $args[list_type] Which type of list to show: "short" (default), "long", or "text"
	 *
	 * @return array An array of Jp_Multi_Event objects
	 */
	public function print_event_list ( array $atts=[] ) : string {
		$eventsCollection = new Jp_Multi_EventCollection();

		if ( isset( $atts['limit'] ) )
			$eventsCollection->setLimit( $atts['limit'] );

		if ( isset( $atts['onlyTags'] ) )
			$eventsCollection->setOnlyShowTags( $atts['onlyTags'] );

		if ( isset( $atts['respectGroupSetting'] ) )
			$eventsCollection->setGroupOccassions( $atts['respectGroupSetting'] );

		if ( isset( $atts['hide_parts'] ) && ! empty( $atts['hide_parts'] ) )
			$eventsCollection->setHiddenEventParts( $atts['hide_parts'] );

		if ( isset( $atts['futureOrPast'] ) && $atts['futureOrPast'] == 'past' )
			$eventsCollection->setToPastOnly();
		else $eventsCollection->setToFutureOnly();

		$eventListOptions = [];
		if ( isset( $atts['list_type'] ) && ! empty( $atts['list_type'] ) )
			$eventListOptions['list_type'] = $atts['list_type'];
		if ( isset( $atts['no_events_text'] ) && ! empty( $atts['no_events_text'] ) )
			$eventListOptions['no_events_text'] = $atts['no_events_text'];
		if ( isset( $atts['show_paging'] ) )
			$eventListOptions['show_paging'] = $atts['show_paging'];

		return $eventsCollection->showEventList( $eventListOptions );
	}


	// function count_upcoming_events () {}


	/**
	 * Print the event calendar grid
	 *
	 * @param array $atts
	 * @param string $atts['tag']  comma-sep list of tag slugs to limit by
	 * @param string $atts['date']  month to show (around this date). Accepts any date format
	 * @param bool $atts['showFilters'] whether to show the filters above the calendar
	 *
	 * @return string calendar html
	 */
	public function print_calendar ( array $atts=[] ) : string {
		$atts = array_merge([
			'tag' => '',
			'date' => 'today',
			'showFilters' => true,
			'onlyShowTheseTagsInFilterList' => '',
		], $atts);

		$EventCollection = new Jp_Multi_EventCollection( [
			'tag' => $atts['tag'],
		]);

		$showFilters = ( $atts['showFilters'] ) ? true : false;

		$theseTags = [];
		if ( $showFilters ) {
			$theseTags = explode( ',', $atts['onlyShowTheseTagsInFilterList'] );
		}

		if ( $showFilters ) {
			add_filter('jpme_cal_grid_top', function ( $html ) use ( $theseTags ) {
				$html .= Jp_Multi_Events::instance()->print_tag_filters( $theseTags );
				return $html;
			});
		}

		$html = $EventCollection->showCalendar( $atts['date'], $showFilters );

		return $html;
	}


	public function print_tag_filters ( array $onlyShowTheseTags=array() ) : string {

		$term_query_args = array(
			'taxonomy' => 'event-tag',
			// 'hide-empty' => true,
		);

		if ( ! empty( $onlyShowTheseTags ) ) :
			if ( ! is_array( $onlyShowTheseTags ) )
				$onlyShowTheseTags = explode( ',', $onlyShowTheseTags );

			array_walk( $onlyShowTheseTags, 'trim' );
			$onlyShowTheseTags = array_filter( $onlyShowTheseTags );

			if ( ! empty( $onlyShowTheseTags ) )
				$term_query_args['slug'] = $onlyShowTheseTags;
		endif;

		$terms = get_terms( $term_query_args );

		$out = $css = '';
		if ( ! is_wp_error( $terms ) && ! empty( $terms ) ) {

			$styles = [];

			$out .= '<div class="event-tag-filters jpme-cal-grid_filters">
				<b class="event-tag-helptext">Filter by Tag</b><br>
				<span style="font-weight:200">Show:</span> 
				';
			$checkedFilters = [];
			if ( isset( $_GET['jpme_tagFilters'] ) ) {
				$checkedFilters = preg_replace( '/[^a-zA-Z0-9,-_]/', '', $_GET['jpme_tagFilters'] );
				$checkedFilters = explode( ',', $checkedFilters );
			}
			foreach ( $terms as $term ) {
				$slug = "tag-filter-{$term->slug}";
				$checked = in_array( $term->slug, $checkedFilters ) ? ' checked ' : '';
				$color = get_term_meta( $term->term_id, 'tag-color', true );
				if ( $color ) {
					$styles[ $term->slug ] = $color;
				}

				$out .= '<label for="' . $slug . '" style="cursor:pointer;white-space:inline-pre;">
					<input type="checkbox" ' . $checked . ' name="' . $slug . '" id="' . $slug . '" value="' . $term->slug . '"> ' . $term->name . '
				</label> ';
			}

			$out .= '</div>';

			if ( count( $styles ) ) {
				$css = '<style>';
				foreach ( $styles as $term => $color ) {
					$css .= '
					#jpme-cal-grid a[data-tags~="' . $term . '"] ,
					#jpme-cal-grid label[for="tag-filter-' . $term . '"]
					{
						background: ' . $color . ';
					}
					';
				}
				$css .= '</style>';
			}
		}

		return $css . $out;
	}

	/**
	 * Find out whether a post/post_id is for one of our events
	 *
	 * @param int|string|WP_Post $post post id or WP_Post to check. Defaults to current post
	 *
	 * @return bool
	 */
	public static function isEvent ( $post=null ): bool {
		return get_post_type( $post ) == 'event';
	}
}
