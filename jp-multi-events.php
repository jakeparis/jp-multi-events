<?php
/*
Plugin Name: JP Multi-Events
Description: Adds Events type and some associated functions
Version: 5.3.1
Author: Jake Paris
Author URI: https://jakeparis.com
Plugin URI: https://gitlab.com/jakeparis/jp-multi-events
Update URI: https://gitlab.com/jakeparis/jp-multi-events
Requires PHP: 7.4
Requires at least: 5.0.0
*/


define( 'JP_MULTI_EVENTS_PLUGIN_VERSION', '5.3.1' );
define( 'JP_MULTI_EVENTS_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'JP_MULTI_EVENTS_PLUGIN_URL', plugins_url( '/', __FILE__ ) );

foreach ( glob( JP_MULTI_EVENTS_PLUGIN_DIR . '/inc/*.php' ) as $f ) {
	require_once $f;
}


Jp_Multi_Events::instance();
Jp_Multi_Events_Admin::instance();

/**
 * Updater
 */
require JP_MULTI_EVENTS_PLUGIN_DIR . 'lib/updater/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/jakeparis/jp-multi-events',
	__FILE__, //Full path to the main plugin file or functions.php.
	'jp-multi-events/jp-multi-events.php'
);
