=== JP Multi Events ===

Contributors: jakeparis
Donate link: https://jakeparis.com/
Tags: events, 
Requires at least: 5.8
Tested up to: 6.6.1
Stable tag: 5.0.0
Requires PHP: 7.4

Provides events for WordPress. 

== Description ==

Provides events in WordPress, as well as some blocks/widgets to display them easily.

= Features =

* Events can have multiple dates
* A single event with multiple dates can optionally set to only 
  display the closest instance in lists. 
* featured images

== Use ==

== Installation ==


== ChangeLog ==

= 5.3.1 =

* No use facing changes, bumped version

= 5.3.0 =

* fixed title filter
* added a few convenience functions like isAllDay(), isEvent()
* improved admin menu icon

= 5.2.0 =

* Prevent accidental clicks to event lists previewed within the block editor

= 5.1.1 =

* Update icons

= 5.0.0 =

**Features**
* Added location field and corresponding maps to events
* Added much more customization display for event listings
* Added paging for event listings
* Added clickable list of event tags below event

**Fixes**
* Fixed and updated some styling, particularly in lists
* Fixed bug where Group Occassions setting didn't stick in the block editor

= 4.1.0 =

* tested up to WP 6.4.2

= 4.0.1 =

* Fix for broken Event Meta block: import lodash.keyby, instead of relying on 
  the lodash library to be in WP window element.
* Fix for broken Event Calendar grid block: updated ServerSideRender block import
  location.
* fix typo

= 4.0.0 =

* Move the date/time input to the center area of the block editor

= 3.3.1 = 

* Fix issue when events with multiple occurences aren't counted properly into the events list limit
* Other minor fixes

= 3.3.0 =

* compatibility fix for WordPress 6.1 (asset scripts not loading)

= 3.2.0 = 

* Event tags in lists now are links to tag permalinks
* Fix a few php8 warnings of wrong empty types.
* Fix styling in empty cells in calendar grid.

= 3.1.0 =

* Added shortcodes for backwards compat and for theme builders which ignore the block editor. 

= 3.0.0 =

* Past events now show on Calendar grid
* Refactor of plugin

= 2.3.0 = 

* Added the ability to set custom colors for the tag filters and the corresponding calendar-grid events. 

= 2.2.4 = 

* Fixed bug where first event date in list might be a past date

= 2.2.3 =

* Fixed bug where not all events show on calendar grid

= 2.2.2 =

* Added backwards compat for some themes which called our function directly

= 2.2.0 =

* Add ability for events listing to show past events (and sort reverse)

= 2.1.0 =

* Allow the Event Listing block to be filtered by tags

= 2.0.2 =

* Fix event tags not showing up in block editor

= 2.0.1 =

* Fix bug where admin calendar popup didn't work in classic editor
* Fix bug where empty filter-tag box resulted in NO filter box instead of
  all the tags

= 2.0.0 =

* Enhanced the Calendar block
  * Now can select a single taxonomy to show
  * Can choose only some of the tag filters to display (and whether to display the filter bar at all)

= 1.8.0 =

* Updated my website url

= 1.7.0 =

* Added a Calendar Grid block.
* Allow old events to continue to display. Now, to remove old events, it is a 
  manual process. 

= 1.6.1 = 
Update to the updater code, mostly to provide compatibility with new 
auto-update feature.

= 1.6.0 = 
Updated the date picker for compatibilty with WordPress 5.5. Also some
housekeeping of plugin files. 

= 1.5.1 =
Added the event dates to the admin listing of Events.

= 1.5.0 =
 
Updates and bugfixes. More resilient styles.

= 1.4.3 =

Test updater location move.

= 1.4.2 =

Move updater location.

= 1.4.1 = 

Test updater

= 1.4.0 =

Added self-updater.

= 1.3.3 =

Basic functionality
